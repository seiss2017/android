package com.lbl.tempchat.common.request;

/**
 * Created by 13616 on 2017/4/28.
 */

public class StatusCode {
    /**
     * 请求成功
     */
    public static final int SUCCESS=200;
    /**
     * 请求失败
     */
    public static final int FAILED=400;
    /**
     * 认证错误
     */
    public static final int AUTHENTICATION_FAILED=0;
    /**
     * 参数错误
     */
    public static final int ERROR_DATA=1;
    /**
     * 用户名错误（用户不存在）
     */
    public static final int USER_NULL=2;
    /**
     * 用户名已注册
     */
    public static final int USER_EXISTS=3;
    /**
     * 密码错误
     */
    public static final int PASSWORD_ERROR=4;
    /**
     * 密码太短（6-16）
     */
    public static final int PASSWORD_LENGTH_ERROR=5;
    /**
     * 文件上传失败
     */
    public static final int FILE_UPLOAD_FAILED=6;
    /**
     * 文件过大
     */
    public static final int FILE_TOO_LARGE=7;
    /**
     * 不支持的图片格式
     */
    public static final int UNSUPPORT_IMAGE_FORMAT=8;
    /**
     * 邮箱错误
     */
    public static final int WRONG_EMAIL=9;
    /**
     * 邮箱已存在
     */
    public static final int EMAIL_EXISTS=10;
    /**
     * 验证码验证失败
     */
    public static final int CHECKER_VALIDATE_FAILED=11;
    /**
     * 验证码过期
     */
    public static final int CHECKER_EXPIRED=12;
    /**
     * 邮件发送失败
     */
    public static final int SEND_EMAIL_FAILED=13;
    /**
     * 会话不存在
     */
    public static final int SESSION_NOT_EXIST=14;
    /**
     * 每天的匹配次数已用完
     */
    public static final int MATCH_TIME_LIMITED=15;
    /**
     * 聊天的时间到了
     */
    public static final int TIME_LIMITED=16;
    /**
     * 对不起你是个好人（对方取消/拒绝了会话）
     */
    public static final int SORRY_YOU_ARE_GOOD_GUY=17;
    /**
     * 会话状态不一致
     */
    public static final int SESSION_STATUS_ERROR=18;
}
