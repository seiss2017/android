package com.lbl.tempchat.common.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by 13616 on 2017/4/28.
 */

public class Status<T> implements Serializable{
    private boolean result;

    private int statusCode;

    private T data;


    /**
     * 请求结果
     */
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    /**
     * 状态码
     */
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * 需要让用户知道的数据
     */
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
