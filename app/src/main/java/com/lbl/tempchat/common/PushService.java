package com.lbl.tempchat.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.WindowManager;

import com.lbl.tempchat.R;
import com.lbl.tempchat.chat.activity.ChatActivity;
import com.lbl.tempchat.common.database.entity.CommentMessage;
import com.lbl.tempchat.common.database.entity.PraiseMessage;
import com.lbl.tempchat.common.database.entity.UserInfoMessage;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.SocketTypeCode;
import com.lbl.tempchat.common.request.StatusCode;
import com.lbl.tempchat.common.request.StatusSocket;
import com.lbl.tempchat.common.request.entity.ChatInfo;
import com.lbl.tempchat.common.request.entity.ChatMessage;
import com.lbl.tempchat.common.request.entity.ChatSendMessage;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.CommentWithComment;
import com.lbl.tempchat.common.request.entity.CommentWithSquare;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.GsonUtil;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

public class PushService extends Service {


    private final static int CONNECTION_CLOSE=1000;


    public static WebSocketClient client;

    public static RequestHandler chatHandler;

    public static RequestHandler commentHandler;

    public static RequestHandler praiseHandler;

    public static RequestHandler matchHandler;

    public static Activity nowActivity;

    public AlertDialog dialog;



    public static void setChatHandler(RequestHandler handler){
        chatHandler=handler;
    }

    public PushService() {
    }

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case CONNECTION_CLOSE:
                    initWebSocketClient();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };


    @Override
    public void onCreate() {
        super.onCreate();
        initWebSocketClient();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("lbl","被摧毁");
    }

    public void createNewDialog(ChatInfo chatInfo){
        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        dialog=builder
                .setTitle("有新的聊天请求")
                .setMessage(chatInfo.getUser().getNickname() )
                .setPositiveButton("确定",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                            ChatSendMessage chatSendMessage=new ChatSendMessage(SocketTypeCode.ACCEPT_CHAT,chatInfo.getSessionId(),null);
                            send(chatSendMessage);
                            Intent intent=new Intent();
                            intent.setAction(getString(R.string.activity_chat));
                            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(ChatActivity.CHAT_INFO,chatInfo);
                            if(nowActivity!=null){
                                nowActivity.startActivity(intent);
                            }
                            initWebSocketClient();
                    }
                })
                .setNegativeButton("取消",null)
                .create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕不消失
        if (!dialog.isShowing()){//此时提示框未显示
            dialog.show();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void initWebSocketClient() {
        try {
            client=new WebSocketClient(new URI(String.format(getString(R.string.url_chat),User.getUser().getUserToken(this)))) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    Log.i("lbl","连接成功");
                }

                @Override
                public void onMessage(String s) {
                    Log.i("lbl","接收消息:"+s);
                    handleMessage(s);
                }

                @Override
                public void onClose(int i, String s, boolean b) {
                    Log.i("lbl","连接关闭");
                    //之后要继续连接
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5000);
                                handler.sendEmptyMessage(CONNECTION_CLOSE);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }

                @Override
                public void onError(Exception e) {
                    Log.i("lbl","连接出错:e"+e.getMessage());
                    e.printStackTrace();
                    //继续连接
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5000);
                                handler.sendEmptyMessage(CONNECTION_CLOSE);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }
            };
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        client.connect();
    }

    public static boolean send(ChatSendMessage chatSendMessage){
        if(client==null||client.getConnection()==null){
            return false;
        }
        else {
            client.send(GsonUtil.getGsonUtil().getGson().toJson(chatSendMessage));
            return true;
        }
    }

    public void handleMessage(String jsonString){
        try {
            JSONObject jsonObject=new JSONObject(jsonString);
            int type=jsonObject.getInt("type");
            switch (type){
                case SocketTypeCode.REQUEST_STATUS:
                    handleRequestStatus(jsonString);
                    break;
                case SocketTypeCode.RECEIVER_CHAT:
                    handleChat(jsonString);
                    break;
                case SocketTypeCode.RECEIVER_COMMENT:
                    handleComment(jsonString);
                    break;
                case SocketTypeCode.RECEIVER_PRAISE:
                    handlePraise(jsonString);
                    break;
                case SocketTypeCode.CHAT_SURE:
                    Looper.prepare();
                    handleChatSure(jsonString);
                    Looper.loop();
                    break;
                case SocketTypeCode.ALIVE_ENSURE:
                    handleAlive();
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void handleRequestStatus(String jsonString){
        try {
            JSONObject jsonObject=new JSONObject(jsonString);
            boolean isResult=jsonObject.getBoolean("result");
            int statusCode=jsonObject.getInt("statusCode");
            String data=jsonObject.getString("data");
            if(statusCode== StatusCode.SORRY_YOU_ARE_GOOD_GUY){
                if(chatHandler!=null){
                    chatHandler.sendEmptyMessage(RequestCode.FINISH_CHAT);
                }

                //TODO:match
                if(matchHandler!=null){
                    matchHandler.sendEmptyMessage(RequestCode.TARGET_REJECT_CHAT);
                }
            }
            if(statusCode==StatusCode.SUCCESS){
                switch (data){
                    case "对方已同意聊天":
                        if(matchHandler!=null){
                            matchHandler.sendEmptyMessage(RequestCode.TARGET_ACCEPT_CHAT);
                        }else {
                            Log.i("lbl","matcherHandler为空");
                        }
                        break;
                    case "发送成功":
                        if(chatHandler!=null){
                            chatHandler.sendEmptyMessage(RequestCode.SEND_CHAT_MESSAGE_SUCCESS);
                        }

                }
            }

            /*if(isResult&&data.equals("发送成功")){
                chatHandler.sendEmptyMessage(RequestCode.SEND_CHAT_MESSAGE_SUCCESS);
            }else if(data.equals("发送成功")){
                chatHandler.sendEmptyMessage(RequestCode.SEND_CHAT_MESSAGE_FAILURE);
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void handleChat(String jsonString){
        if(chatHandler==null){
            Log.i("lbl","chatHandler为null");
            return;
        }
        StatusSocket<ChatMessage> statusSocket=GsonUtil.getGsonUtil().getGson()
                .fromJson(jsonString, GsonUtil.getResponseType(RequestCode.GET_CHAT_RECEIVER_MESSAGE_SUCCESS));
        if(statusSocket.isResult()==false){
            if(!chatHandler.handlerErrorMessage(statusSocket.getStatusCode())){
                Log.i("lbl","接收消息失败");
            }
            return;
        }
        Message message=new Message();
        message.what=RequestCode.GET_CHAT_RECEIVER_MESSAGE_SUCCESS;
        Bundle bundle=new Bundle();
        bundle.putSerializable(ChatActivity.RECEIVER_MESSAGE,statusSocket.getData());
        message.setData(bundle);
        chatHandler.sendMessage(message);
    }


    public void handleComment(String jsonString){
        JSONObject jsonObject= null;
        try {
            jsonObject = new JSONObject(jsonString);
            boolean isResult=jsonObject.getBoolean("result");
            if(isResult==false){
                Log.i("lbl","接收消息失败:"+jsonString);
                return;
            }

            JSONObject dataJSONObject=jsonObject.getJSONObject("data");
            if(dataJSONObject.has("pointTo")){
                StatusSocket<CommentWithComment> statusSocket=GsonUtil.getGsonUtil().getGson()
                        .fromJson(jsonString, GsonUtil.getResponseType(RequestCode.RECEIVER_COMMENT_WITH_COMMENT_NOTIFICATION));
                new CommentMessage(statusSocket.getData()).save();

            }else {
                StatusSocket<CommentWithSquare> statusSocket=GsonUtil.getGsonUtil().getGson()
                        .fromJson(jsonString, GsonUtil.getResponseType(RequestCode.RECEIVER_COMMENT_WITH_SQUAREMESSAGE_NOTIFICATION));
                new CommentMessage(statusSocket.getData()).save();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        if(commentHandler!=null){
            commentHandler.sendEmptyMessage(RequestCode.RECEIVER_COMMENT_NOTIFICATION_SUCCESS);
        }
    }

    /**
     * 处理赞的信息
     * @param jsonString
     */
    public void handlePraise(String jsonString){
        StatusSocket<com.lbl.tempchat.common.request.entity.PraiseMessage> statusSocket=GsonUtil.getGsonUtil().getGson()
                .fromJson(jsonString, GsonUtil.getResponseType(RequestCode.RECEIVER_PRAISE_NOTIFICATION_SUCCESS));
        if(statusSocket.isResult()==false){
            Log.i("lbl","接收消息失败:"+jsonString);
            return;
        }
        new PraiseMessage(statusSocket.getData()).save();
        if(praiseHandler!=null){
            praiseHandler.sendEmptyMessage(RequestCode.RECEIVER_PRAISE_NOTIFICATION_SUCCESS);
        }

    }

    //处理会话确认,即是否要聊天
    public void handleChatSure(String jsonString){
        StatusSocket<ChatInfo> statusSocket=GsonUtil.getGsonUtil().getGson()
                .fromJson(jsonString, GsonUtil.getResponseType(RequestCode.ACCEPT_CHAT));
        createNewDialog(statusSocket.getData());

    }

    //处理保持聊天的请求
    public void handleAlive(){
        if(chatHandler==null){
            return;
        }
        chatHandler.sendEmptyMessage(RequestCode.SEND_ALIVE_MESSAGE);
    }
}
