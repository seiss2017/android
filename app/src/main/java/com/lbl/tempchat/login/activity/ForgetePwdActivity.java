package com.lbl.tempchat.login.activity;

import android.graphics.Color;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.user.activity.ChangePasswordActivity;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.CheckUtils;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class ForgetePwdActivity extends BaseActivity {

    @BindView(R.id.tiet_forget_password_email)
    TextInputEditText tiet_forget_password_email;

    @BindView(R.id.tiet_froget_password_password)
    TextInputEditText tiet_froget_password_password;

    @BindView(R.id.tiet_forget_password_new_password_again)
    TextInputEditText tiet_forget_password_new_password_again;

    @BindView(R.id.tiet_forget_password_check)
    TextInputEditText tiet_forget_password_check;


    private RequestBase<String> requestChecker;

    private RequestBase<String> requestForgetPassword;

    private OkHttpClient httpClient;


    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_CHECKER_SUCCESS:
                    User.getUser().setUserCipher(ForgetePwdActivity.this,requestChecker.getStatus().getData());
                    break;
                case RequestCode.GET_CHECKER_FAILURE:
                    Toast.makeText(ForgetePwdActivity.this,"获取验证码失败,请稍后重试",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.FORGET_PASSWORD_SUCCESS:
                    User.getUser().setUserToken(ForgetePwdActivity.this,requestForgetPassword.getStatus().getData());
                    new AlertDialog.Builder(ForgetePwdActivity.this)
                            .setTitle("修改密码成功")
                            .setPositiveButton("确定", (a,b)->{a.dismiss();ForgetePwdActivity.this.finish();})
                            .create()
                            .show();
                    break;
                case RequestCode.FORGET_PASSWORD_FAILURE:
                    Toast.makeText(ForgetePwdActivity.this,"修改密码失败,请稍后重试",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgete_pwd);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }


    @Override
    protected void initVariables(){
        requestChecker=new RequestBase<>(getString(R.string.url_checker),
                RequestCode.GET_CHECKER_SUCCESS,
                RequestCode.GET_CHECKER_FAILURE);

        requestForgetPassword=new RequestBase<>(getString(R.string.url_forget_password),
                RequestCode.FORGET_PASSWORD_SUCCESS,
                RequestCode.FORGET_PASSWORD_FAILURE);

        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_forget_password);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("忘记密码");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.btn_forget_password_request_check)
    public void requestChecker(){
        String email=tiet_forget_password_email.getText().toString();
        if(!CheckUtils.isEmail(email)){
            Toast.makeText(this,"请输入正确的邮箱",Toast.LENGTH_SHORT).show();
            return;
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("email",email);
        requestChecker.requestPost(this,handler,httpClient,map);
    }

    @OnClick(R.id.btn_forget_password)
    public void forgetPwd(){
        String email=tiet_forget_password_email.getText().toString();
        String newPwd=tiet_froget_password_password.getText().toString();
        String newPwdAgain=tiet_forget_password_new_password_again.getText().toString();
        String checker=tiet_forget_password_check.getText().toString();
        if(!CheckUtils.isEmail(email)
                ||newPwd.equals("")
                ||newPwdAgain.equals("")
                ||checker.equals("")){
            Toast.makeText(this,"请输入完整信息",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!newPwd.equals(newPwdAgain)){
            Toast.makeText(this,"两次密码要保持一致",Toast.LENGTH_SHORT).show();
            return;
        }
        if(newPwd.length()<6||newPwd.length()>20){
            Toast.makeText(this,"密码长度应为6-20位",Toast.LENGTH_SHORT).show();
            return;
        }

        HashMap<String,String> map=new HashMap<>();
        map.put("email",email);
        map.put("passwd",newPwd);
        map.put("checker",checker);
        map.put("cipher",User.getUser().getUserCipher(this));
        requestForgetPassword.requestPost(this,handler,httpClient,map);

    }


}
