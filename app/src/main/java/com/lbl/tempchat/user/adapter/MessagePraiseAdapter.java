package com.lbl.tempchat.user.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.lbl.tempchat.R;
import com.lbl.tempchat.common.database.entity.PraiseMessage;
import com.lbl.tempchat.common.database.entity.UserInfoMessage;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by 13616 on 2017/4/10.
 */

public class MessagePraiseAdapter extends BaseAdapter {
    private List<PraiseMessage> praiseList;

    private Activity activity;

    public MessagePraiseAdapter(Activity activity){
        this.activity=activity;
        praiseList=new ArrayList<>();
    }

    public void add(List<PraiseMessage> praiseMessageList){
       this.praiseList=praiseMessageList;
        Collections.sort(praiseList);
    }


    @Override
    public int getCount() {
        return praiseList.size();
    }

    @Override
    public Object getItem(int position) {
        return praiseList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView=LayoutInflater.from(activity).inflate(R.layout.item_message_praise, null);
        CircleImageView iv_message_praise_head=(CircleImageView)rootView.findViewById(R.id.iv_message_praise_head);
        TextView tv_message_praise_nickname=(TextView)rootView.findViewById(R.id.tv_message_praise_nickname);
        TextView tv_message_praise_time=(TextView)rootView.findViewById(R.id.tv_message_praise_time);
        CircleImageView iv_message_comment_user_head=(CircleImageView)rootView.findViewById(R.id.iv_message_comment_user_head);
        TextView tv_message_comment_user_nickname=(TextView)rootView.findViewById(R.id.tv_message_comment_user_nickname);
        TextView tv_message_comment_user_content=(TextView)rootView.findViewById(R.id.tv_message_comment_user_content);
        PraiseMessage praiseMessage=praiseList.get(position);

        UserInfoMessage liker=(UserInfoMessage)new Select().from(UserInfoMessage.class).where("id=?",praiseMessage.liker).execute().get(0);
        UserInfoMessage poster=(UserInfoMessage)new Select().from(UserInfoMessage.class).where("id=?",praiseMessage.poster).execute().get(0);

        if(liker.avatar!=null&&!liker.avatar.equals("")){
            Picasso.with(activity).load(liker.avatar).into(iv_message_praise_head);
        }
        tv_message_praise_nickname.setText(liker.nickname);
        tv_message_praise_time.setText(praiseMessage.time.replace("T"," "));
        if(poster.avatar!=null&&!poster.avatar.equals("")){
            Picasso.with(activity).load(poster.avatar).into(iv_message_comment_user_head);
        }
        tv_message_comment_user_nickname.setText(poster.nickname);
        tv_message_comment_user_content.setText(praiseMessage.content);
        return rootView;
    }

    class ViewHolder{
        @BindView(R.id.iv_message_praise_head)
        CircleImageView iv_message_praise_head;

        @BindView(R.id.tv_message_praise_nickname)
        TextView tv_message_praise_nickname;

        @BindView(R.id.tv_message_praise_time)
        TextView tv_message_praise_time;

        @BindView(R.id.iv_message_comment_user_head)
        CircleImageView iv_message_comment_user_head;

        @BindView(R.id.tv_message_comment_user_nickname)
        TextView tv_message_comment_user_nickname;

        @BindView(R.id.tv_message_comment_user_content)
        TextView tv_message_comment_user_content;
        public ViewHolder(View view){
            ButterKnife.bind(this,view);
        }
    }
}