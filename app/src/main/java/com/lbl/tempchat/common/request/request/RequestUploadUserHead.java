package com.lbl.tempchat.common.request.request;

import android.app.Activity;

import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by 13616 on 2017/4/30.
 */

public class RequestUploadUserHead extends RequestBase<String> {
    public RequestUploadUserHead(String url, int successCode, int failureCode){
        super(url,successCode, failureCode);
    }

    public void requestPostFile(Activity activity, RequestHandler handler, OkHttpClient httpClient, HashMap<String,String> map,File file){
        RequestBody requestBodyTest= FormBody.create(MediaType.parse("image/jpeg"),file);
        MultipartBody.Builder builder=new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("avatar", file.getName(),requestBodyTest);
        for(Map.Entry<String,String> entry:map.entrySet()){
            builder.addFormDataPart(entry.getKey(),entry.getValue());
        }
        RequestBody requestBody=builder.build();
        Request request=new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        request(handler,httpClient,request);
    }


}
