package com.lbl.tempchat.user.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Select;
import com.lbl.tempchat.R;
import com.lbl.tempchat.common.PullListView.XListView;
import com.lbl.tempchat.common.PushService;
import com.lbl.tempchat.common.database.entity.CommentMessage;
import com.lbl.tempchat.common.database.entity.PraiseMessage;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.user.adapter.MessagePraiseAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MessagePraiseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessagePraiseFragment extends Fragment {
    private MessagePraiseAdapter messagePraiseAdapter;
    private List<PraiseMessage> praiseMessageList;

    private RequestHandler handler=new RequestHandler(this.getActivity()){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.RECEIVER_PRAISE_NOTIFICATION_SUCCESS:
                    handleReceiverPraise();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    public MessagePraiseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment MessagePraiseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MessagePraiseFragment newInstance() {
        MessagePraiseFragment fragment = new MessagePraiseFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        messagePraiseAdapter=new MessagePraiseAdapter(getActivity());
        handler.setActivity(this.getActivity());
        PushService.praiseHandler=handler;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_message_praise, container, false);
        XListView lv_message_praise=(XListView)rootView.findViewById(R.id.lv_message_praise);
        lv_message_praise.setPullLoadEnable(false);
        lv_message_praise.setPullRefreshEnable(false);
        lv_message_praise.setAutoLoadEnable(false);
        lv_message_praise.setAdapter(messagePraiseAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        PushService.praiseHandler=handler;
        handleReceiverPraise();
    }

    public void handleReceiverPraise(){
        praiseMessageList=new Select().from(PraiseMessage.class).orderBy("id desc").execute();
        messagePraiseAdapter.add(praiseMessageList);
        messagePraiseAdapter.notifyDataSetChanged();
    }
}
