package com.lbl.tempchat.square.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by 13616 on 2017/4/9.
 */

public class MomentDetailCommentAdapter extends BaseAdapter {
    private ArrayList<Comment> commentList;

    private Activity activity;

    public MomentDetailCommentAdapter(Activity activity){
        this.activity=activity;
        commentList=new ArrayList<>();
    }

    public void add(ArrayList<Comment> commentList){
        HashSet<Integer> existValue=new HashSet<>();
        for(Comment comment:this.commentList){
            existValue.add(comment.getId());
        }
        for(Comment comment:commentList){
            if(!existValue.contains(comment.getId())){
                this.commentList.add(comment);
                existValue.add(comment.getId());
            }
        }
    }

    public void addFirst(Comment comment){
        commentList.add(comment);
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView!=null){
            viewHolder=(ViewHolder)convertView.getTag();
        }else {
            convertView= LayoutInflater.from(activity).inflate(R.layout.item_comment, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        Comment comment=commentList.get(position);
        if(comment.getReviewer().getAvatar()!=null){
            Picasso.with(activity).load(comment.getReviewer().getAvatar()).into(viewHolder.iv_comment_head);
        }
        viewHolder.tv_comment_nickname.setText(comment.getReviewer().getNickname());
        viewHolder.tv_comment_time.setText(comment.getTime().replace("T"," "));
        viewHolder.tv_comment_content.setText(comment.getPointTo()==null?
                comment.getContent()
                :"回复 "+comment.getPointTo().getNickname()+":"+comment.getContent());

        return convertView;
    }

    class ViewHolder{
        @BindView(R.id.iv_comment_head)
        CircleImageView iv_comment_head;

        @BindView(R.id.tv_comment_nickname)
        TextView tv_comment_nickname;

        @BindView(R.id.tv_comment_content)
        TextView tv_comment_content;

        @BindView(R.id.tv_comment_time)
        TextView tv_comment_time;


        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
