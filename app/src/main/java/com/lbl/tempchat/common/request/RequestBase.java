package com.lbl.tempchat.common.request;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.utils.GsonUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 13616 on 2017/4/28.
 */

public class RequestBase<T> {


    protected String url;

    protected int successCode;

    protected int failureCode;

    public RequestBase(String url,int successCode,int failureCode){
        this.url=url;
        this.successCode=successCode;
        this.failureCode=failureCode;
    }


    protected Status<T> status;

    public Status<T> getStatus(){
        return status;
    }

    protected void request(RequestHandler handler, OkHttpClient httpClient,Request request){
        Call call=httpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handlerError(handler);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response==null||response.isSuccessful()==false){
                    handlerError(handler);
                    return;
                }
                Gson gson=GsonUtil.getGsonUtil().getGson();
                String str_response=response.body().string();
                Log.i("lbl","response:"+str_response);
                try {
                    if((status= gson.fromJson(str_response, GsonUtil.getResponseType(successCode)))==null){
                        handlerError(handler);
                        return;
                    }
                    if(status.isResult()==false){
                        if(!handler.handlerErrorMessage(status.getStatusCode())){
                            Log.i("lbl","handlereror 返回了false");
                            handlerError(handler);
                        }
                        return;
                    }
                    handlerSuccess(handler);
                }catch (JsonSyntaxException e){
                    Log.i("lbl","json解析异常");
                    handlerError(handler);
                    e.printStackTrace();
                }

            }
        });
    }
    public void requestGet(Activity activity, RequestHandler handler, OkHttpClient httpClient){
        Request request=new Request.Builder()
                .url(url)
                .get()
                .build();
        request(handler,httpClient,request);
    }

    public void requestPost(Activity activity, RequestHandler handler, OkHttpClient httpClient, HashMap<String,String> map){
        FormBody.Builder builder=new FormBody.Builder();
        for(Map.Entry<String,String> entry:map.entrySet()){
            builder.add(entry.getKey(),entry.getValue());
        }
        FormBody formBody=builder.build();
        Request request=new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        request(handler,httpClient,request);
    }

    private void handlerError(RequestHandler handler){
        handler.sendEmptyMessage(failureCode);
    }

    private void handlerSuccess(RequestHandler handler){
        handler.sendEmptyMessage(successCode);
    }



}
