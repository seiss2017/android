package com.lbl.tempchat.common.request.entity;

import java.io.Serializable;

/**
 * Created by 13616 on 2017/5/4.
 */

public class ChatInfo implements Serializable {
    private int sessionId;

    private UserInfo user;


    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }
}
