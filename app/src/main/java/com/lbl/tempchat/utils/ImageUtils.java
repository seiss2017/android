package com.lbl.tempchat.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.FileNotFoundException;

/**
 * Created by 13616 on 2017/4/29.
 */

public class ImageUtils {

    public static final int PHOTO_REQUEST_CAMERA = 1001;// 拍照
    public static final int PHOTO_REQUEST_GALLERY = 1002;// 从相册中选择
    public static final int PHOTO_REQUEST_CUT = 1003;// 结果

    //设置相机所获取的照片的名字
    public static final String PHOTO_FILE_NAME = "temp_head_photo.jpg";

    //设置存储的头像的逃跑名字
    public static final String PHTOT_FILE_NAME_IN_APP="com.lbl.tempchat/photo_file_name_in_app.jpeg";

    public static void crop(Uri uri, Uri newUri, Activity activity){
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 200);
        // 图片格式
        intent.putExtra(MediaStore.EXTRA_OUTPUT, newUri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        activity.startActivityForResult(intent, PHOTO_REQUEST_CUT);

    }

    public static Bitmap decodeUriAsBitmap(Uri uri,Activity activity){
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    public static boolean hasSdcard(){
        return Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED)
                ? true:false;
    }
}
