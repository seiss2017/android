package com.lbl.tempchat.square.adapter;

import android.app.Activity;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.adapter.MyGraidViewAdapter;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.common.view.MyGridView;
import com.lbl.tempchat.user.info.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

/**
 * Created by 13616 on 2017/4/8.
 */

public class MomentAdapter extends BaseAdapter {
    private ArrayList<SquareMessage> list;

    //private HashMap<Integer,Boolean> map;

    private Activity activity;

    private RequestBase<Integer> requestPraise;

    private boolean isFinishRequest=true;

    private OkHttpClient httpClient;

    private int likeId=-1;

    private RequestHandler handler=new RequestHandler(activity){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.SQUARE_MESSAGE_PRAISE_SUCCESS:
                    for(int i=0;i<list.size();i++){
                        if(list.get(i).getId()==likeId){
                            list.get(i).setLikes(requestPraise.getStatus().getData());
                            list.get(i).setHasLiked(true);
                        }
                    }
                    //map.put(likeId,true);
                    notifyDataSetChanged();
                    break;
                case RequestCode.SQUARE_MESSAGE_PRAISE_FAILURE:
                    if(activity.isFinishing()==false){
                        Toast.makeText(activity,"点赞失败",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_SUCCESS:
                    for(int i=0;i<list.size();i++){
                        if(list.get(i).getId()==likeId){
                            list.get(i).setLikes(requestPraise.getStatus().getData());
                            list.get(i).setHasLiked(false);
                        }
                    }
                    notifyDataSetChanged();
                    break;
                case RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_FAILURE:
                    if(activity.isFinishing()==false){
                        Toast.makeText(activity,"取消点赞失败",Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
            isFinishRequest=true;
        }

    };


    public MomentAdapter(Activity activity){
        this.activity=activity;
        list=new ArrayList<>();
        handler.setActivity(activity);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();

    }


    public void add(ArrayList<SquareMessage> squareMessageList){
        HashSet<Integer> existValue=new HashSet<>();
        for(SquareMessage msg:list){
            existValue.add(msg.getId());
        }
        for(SquareMessage msg:squareMessageList){
            if(!existValue.contains(msg.getId())){
                existValue.add(msg.getId());
                list.add(msg);
                //map.put(msg.getId(),false);
            }
        }
        Collections.sort(list);
    }

    public void clear(){
        list.clear();
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView!=null){
            viewHolder = (ViewHolder) convertView.getTag();
        }else {
            convertView= LayoutInflater.from(activity).inflate(R.layout.item_moment, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        SquareMessage squareMessage=list.get(position);
        viewHolder.tv_moment_nickname.setText(squareMessage.getPoster().getNickname());
        if(squareMessage.getPoster().getAvatar()!=null
                &&!squareMessage.getPoster().getAvatar().equals("")){
            Picasso.with(activity).load(squareMessage.getPoster().getAvatar()).into(viewHolder.iv_moment_head);
        }
        viewHolder.tv_moment_content.setText(squareMessage.getContent());
        viewHolder.tv_moment_time.setText(squareMessage.getTime().replace("T"," "));
        viewHolder.tv_moment_comment_times.setText(String.valueOf(squareMessage.getComments()));
        viewHolder.tv_moment_praise_times.setText(String.valueOf(squareMessage.getLikes()));
        viewHolder.iv_moment_praise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFinishRequest){
                    return;
                }
                isFinishRequest=false;
                if(squareMessage.isHasLiked()){
                    requestPraise=new RequestBase<>(String.format(activity.getString(R.string.url_square_message_cancel_praise),squareMessage.getId()),
                            RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_SUCCESS,
                            RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_FAILURE);
                }else {
                    requestPraise=new RequestBase<>(String.format(activity.getString(R.string.url_square_message_praise),squareMessage.getId()),
                            RequestCode.SQUARE_MESSAGE_PRAISE_SUCCESS,
                            RequestCode.SQUARE_MESSAGE_PRAISE_FAILURE);
                }

                HashMap<String,String> post=new HashMap<String, String>();
                post.put("token",User.getUser().getUserToken(activity));
                likeId=squareMessage.getId();
                requestPraise.requestPost(activity,handler,httpClient,post);

            }
        });

        if(squareMessage.isHasLiked()){
            viewHolder.iv_moment_praise.setImageResource(R.drawable.moment_praise_pressed);
        }else {
            viewHolder.iv_moment_praise.setImageResource(R.drawable.moment_praise);
        }

        MyGraidViewAdapter adapter=new MyGraidViewAdapter(activity);
        adapter.add(squareMessage.getImages());
        Boolean isTheSame=true;
        if(viewHolder.gv_moment_photos.getAdapter()==null
                ||(viewHolder.gv_moment_photos.getAdapter()!=null&&viewHolder.gv_moment_photos.getAdapter().getCount()!=adapter.getCount())){
            isTheSame=false;
        }
        if(isTheSame){
            for(int i=0;i<viewHolder.gv_moment_photos.getAdapter().getCount();i++){
                String path=(String) viewHolder.gv_moment_photos.getAdapter().getItem(i);
                if(!path.equals((String)adapter.getItem(i))){
                    isTheSame=false;
                    break;
                }
            }
        }
        //设置图片
        if(!isTheSame){
            viewHolder.gv_moment_photos.setAdapter(adapter);
            if(adapter.getCount()>1){
                viewHolder.gv_moment_photos.setNumColumns(3);
            }else {
                viewHolder.gv_moment_photos.setNumColumns(1);
            }
            adapter.notifyDataSetChanged();
        }


        return convertView;
    }

    class ViewHolder{
        @BindView(R.id.iv_moment_head)
        CircleImageView iv_moment_head;

        @BindView(R.id.tv_moment_nickname)
        TextView tv_moment_nickname;

        @BindView(R.id.tv_moment_time)
        TextView tv_moment_time;

        @BindView(R.id.tv_moment_content)
        TextView tv_moment_content;

        @BindView(R.id.gv_moment_photos)
        MyGridView gv_moment_photos;

        @BindView(R.id.tv_moment_comment_times)
        TextView tv_moment_comment_times;

        @BindView(R.id.tv_moment_praise_times)
        TextView tv_moment_praise_times;

        @BindView(R.id.iv_moment_praise)
        ImageView iv_moment_praise;
        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }

    }


}
