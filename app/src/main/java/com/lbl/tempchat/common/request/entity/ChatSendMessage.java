package com.lbl.tempchat.common.request.entity;

/**
 * Created by 13616 on 2017/5/4.
 */

public class ChatSendMessage {
    public ChatSendMessage(){}

    public ChatSendMessage(int type,int sessionId,String content){
        this.type=type;
        this.sessionId=sessionId;
        this.content=content;
    }

    private int type;

    private int sessionId;

    private String content;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
