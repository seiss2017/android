package com.lbl.tempchat.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 13616 on 2017/4/30.
 */

public class MyDateUtils {
    public static SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


    public static Date formatStringToDate(String date){
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
