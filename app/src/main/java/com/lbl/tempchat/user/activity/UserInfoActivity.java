package com.lbl.tempchat.user.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.common.request.request.RequestUploadUserHead;
import com.lbl.tempchat.common.request.request.RequestUserInfo;
import com.lbl.tempchat.square.activity.SquareActivity;
import com.lbl.tempchat.square.adapter.MomentAdapter;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;


public class UserInfoActivity extends BaseActivity {
    //用来存储选择的头像的文件
    private File tempFile;

    //用来保存修改后的头像的文件
    private File photoFile;
    //头像的bitmap
    private Bitmap bitmap;



    @BindView(R.id.toolbar_user_info) Toolbar toolbar;

    @BindView(R.id.spinner_user_info_sex) Spinner spinner;

    @BindView(R.id.iv_user_info_head)
    CircleImageView iv_user_info_head;

    @BindView(R.id.tiet_user_info_nickname)
    TextInputEditText tiet_user_info_nickname;

    @BindView(R.id.tiet_user_info_introduction)
    TextInputEditText tiet_user_info_introduction;

    //选择头像来源的弹出框
    AlertDialog dialog_user_head_source;

    private OkHttpClient httpClient;

    private RequestBase<UserInfo> requestUserInfo;

    private RequestUploadUserHead requestUploadUserHead;

    private RequestBase<UserInfo> requestModifyUserInfo;


    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_USER_INFO_SUCCESS:
                    setViews();
                    break;
                case RequestCode.GET_USER_INFO_FAILURE:
                    Toast.makeText(UserInfoActivity.this,"获取用户信息失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.UPLOAD_USER_AVATAR_SUCCESS:
                    break;
                case RequestCode.UPLOAD_USER_AVATAR_FAILURE:
                    Toast.makeText(UserInfoActivity.this,"修改头像失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.MODIFY_USER_INFO_SUCCESS:
                    finish();
                    break;
                case RequestCode.MODIFY_USER_INFO_FAILURE:
                    Toast.makeText(UserInfoActivity.this,"修改个人信息失败",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);
            }

        }
    };


    //spinner的数据源
    private static final String[] sexString={"女","男"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }
    @Override
    protected void initVariables(){
        requestUserInfo=new RequestBase<>(String.format(getString(R.string.url_user_info_get), User.getUser().getUserToken(this)),
                RequestCode.GET_USER_INFO_SUCCESS,
                RequestCode.GET_USER_INFO_FAILURE);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
        String local_file =Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+ImageUtils.PHTOT_FILE_NAME_IN_APP;
        photoFile=new File(local_file);
        if(photoFile.getParentFile().exists()==false){
            photoFile.getParentFile().mkdirs();
        }
        requestUploadUserHead=new RequestUploadUserHead(getString(R.string.url_user_avatar),
                RequestCode.UPLOAD_USER_AVATAR_SUCCESS,
                RequestCode.UPLOAD_USER_AVATAR_FAILURE);
        requestModifyUserInfo=new RequestBase<>(getString(R.string.url_user_info),
                RequestCode.MODIFY_USER_INFO_SUCCESS,
                RequestCode.MODIFY_USER_INFO_FAILURE);
    }

    @Override
    protected void initViews(){
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        //设置返回键
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("编辑个人信息");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.icon_cancel);

        //设置性别选择
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,sexString);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadData(){
        requestUserInfo.requestGet(this,handler,httpClient);
    }


    @Override
    protected void setViews(){
        if(requestUserInfo.getStatus().isResult()==false){
            return;
        }
        if(requestUserInfo.getStatus().getData().getAvatar()!=null
                &&!requestUserInfo.getStatus().getData().getAvatar().equals("")){
            Picasso.with(UserInfoActivity.this).load(requestUserInfo.getStatus().getData().getAvatar())
                    .into(iv_user_info_head);
        }
        tiet_user_info_nickname.setText(requestUserInfo.getStatus().getData().getNickname());
        tiet_user_info_introduction.setText(requestUserInfo.getStatus().getData().getIntro());
        if(requestUserInfo.getStatus().getData().getSex().equals(UserInfo.GIRL)){
            spinner.setSelection(0);
        }else {
            spinner.setSelection(1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_user_info_finish:
                modifyUserInfo();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case ImageUtils.PHOTO_REQUEST_GALLERY:
                if (data != null) {
                    // 得到图片的全路径
                    Uri uri = data.getData();
                    ImageUtils.crop(uri,Uri.fromFile(photoFile),this);

                }
                break;
            case ImageUtils.PHOTO_REQUEST_CAMERA:
                if (ImageUtils.hasSdcard()) {
                    tempFile = new File(Environment.getExternalStorageDirectory(),
                            ImageUtils.PHOTO_FILE_NAME);
                    ImageUtils.crop(Uri.fromFile(tempFile),Uri.fromFile(photoFile),this);
                } else {
                    Toast.makeText(UserInfoActivity.this, "未找到存储卡，无法存储照片！", Toast.LENGTH_LONG).show();
                }
                break;

            case ImageUtils.PHOTO_REQUEST_CUT:
                //Toast.makeText(this,"in",Toast.LENGTH_SHORT).show();
                if(data==null) {
                    //Toast.makeText(this,"data is null",Toast.LENGTH_SHORT).show();
                    break;
                }
                if(data.getData()==null){
                    //Toast.makeText(this,"getdata is null",Toast.LENGTH_SHORT).show();
                }
                bitmap = data.getParcelableExtra("data");

                if (photoFile.exists()) {
                    photoFile.delete();
                }
                try {
                    FileOutputStream out = new FileOutputStream(photoFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                    tempFile=photoFile;
                    //获取压缩过后的bitmap
                    bitmap=ImageUtils.decodeUriAsBitmap(Uri.fromFile(photoFile),this);
                    iv_user_info_head.setImageBitmap(bitmap);
                    HashMap<String,String> map=new HashMap<>();
                    map.put("token",User.getUser().getUserToken(this));
                    requestUploadUserHead.requestPostFile(this,handler,httpClient,map,tempFile);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }

    }


    @OnClick(R.id.btn_user_info_head_change)
    public void changeHead(){
        View view_source=View.inflate(UserInfoActivity.this,R.layout.dialog_user_info_head_source,null);
        //从相机获取头像
        final TextView tv_user_info_head_from_camera=(TextView)view_source.findViewById(R.id.tv_user_info_head_from_camera);
        tv_user_info_head_from_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog_user_head_source!=null&&dialog_user_head_source.isShowing()==true){
                    dialog_user_head_source.cancel();
                }
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                // 判断存储卡是否可以用，可用进行存储
                if (ImageUtils.hasSdcard()) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(new File(Environment
                                    .getExternalStorageDirectory(), ImageUtils.PHOTO_FILE_NAME)));
                }
                startActivityForResult(intent, ImageUtils.PHOTO_REQUEST_CAMERA);
            }
        });

        //从相册获取头像
        TextView tv_user_info_head_from_photo=(TextView)view_source.findViewById(R.id.tv_user_info_head_from_photo);
        tv_user_info_head_from_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消弹出框
                if(dialog_user_head_source!=null&&dialog_user_head_source.isShowing()==true){
                    dialog_user_head_source.cancel();
                }
                // 激活系统图库，选择一张图片
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/jpeg,image/jpg");
                startActivityForResult(intent, ImageUtils.PHOTO_REQUEST_GALLERY);
            }
        });
        dialog_user_head_source=new AlertDialog.Builder(UserInfoActivity.this)
                .setTitle("选择头像")
                .setView(view_source)
                .create();
        dialog_user_head_source.show();
    }

    private void modifyUserInfo(){
        String nickname=tiet_user_info_nickname.getText().toString();
        String intro=tiet_user_info_introduction.getText().toString();
        String sex=spinner.getSelectedItemPosition()==0?"m":"g";
        if(nickname==null||nickname.equals("")||nickname.length()>=32){
            Toast.makeText(this,"昵称长度应小于32且不为空",Toast.LENGTH_SHORT).show();
        }
        if(intro==null||intro.length()>=140){
            Toast.makeText(this,"个人签名长度应小于140",Toast.LENGTH_SHORT).show();
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("nickname",nickname);
        map.put("intro",intro);
        map.put("sex",sex);
        map.put("token",User.getUser().getUserToken(this));
        requestModifyUserInfo.requestPost(this,handler,httpClient,map);
    }
}
