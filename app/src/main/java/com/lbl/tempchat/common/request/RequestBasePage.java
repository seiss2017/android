package com.lbl.tempchat.common.request;

import android.app.Activity;

import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.user.info.User;

import java.util.HashMap;

import okhttp3.OkHttpClient;

/**
 * Created by 13616 on 2017/4/30.
 */

public class RequestBasePage<T> extends RequestBase<T> {

    private int row=10;

    private int page=1;


    protected String baseUrl;
    public RequestBasePage(String url, int successCode, int failureCode){
        super(url,successCode,failureCode);
        baseUrl=url;
    }

    @Override
    public void requestGet(Activity activity, RequestHandler handler, OkHttpClient httpClient){
        url=String.format(baseUrl, User.getUser().getUserToken(activity), row, page);
        super.requestGet(activity,handler,httpClient);
    }

    @Override
    public void requestPost(Activity activity, RequestHandler handler, OkHttpClient httpClient, HashMap<String,String> map){
        url=String.format(baseUrl, User.getUser().getUserToken(activity), row, page);
        super.requestPost(activity,handler,httpClient,map);
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        if(row>0)
            this.row = row;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        if(page>0)
            this.page = page;
    }
}
