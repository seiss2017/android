package com.lbl.tempchat.common.request.entity;

import android.support.annotation.NonNull;

import com.activeandroid.query.Select;
import com.lbl.tempchat.common.database.entity.CommentMessage;
import com.lbl.tempchat.common.database.entity.UserInfoMessage;
import com.lbl.tempchat.utils.MyDateUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by 13616 on 2017/4/30.
 */

public class Comment implements Comparable{
    public Comment(){}

    public Comment(CommentMessage comment){
        List<UserInfoMessage> uim=new Select().from(UserInfoMessage.class).where("id=?",comment.pointTo).execute();
        this.pointTo=uim.size()==0?null:new UserInfo(uim.get(0));
        this.id=comment.id;
        this.time=comment.time;
        uim=new Select().from(UserInfoMessage.class).where("id=?",comment.reviewer).execute();
        this.reviewer=uim.size()==0?null:new UserInfo(uim.get(0));
        this.piazzaId=comment.piazzaId;
        this.content=comment.content;
        this.likes=comment.likes;
    }

    private UserInfo pointTo;

    private int id;

    private String time;

    private UserInfo reviewer;

    private int piazzaId;

    private String content;

    private int likes;


    public UserInfo getPointTo() {
        return pointTo;
    }

    public void setPointTo(UserInfo pointTo) {
        this.pointTo = pointTo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public UserInfo getReviewer() {
        return reviewer;
    }

    public void setReviewer(UserInfo reviewer) {
        this.reviewer = reviewer;
    }

    public int getPiazzaId() {
        return piazzaId;
    }

    public void setPiazzaId(int piazzaId) {
        this.piazzaId = piazzaId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Date date1= MyDateUtils.formatStringToDate(time);
        Date date2= MyDateUtils.formatStringToDate(((Comment)o).getTime());
        return date1.getTime()>date2.getTime()?-1:1;
    }
}
