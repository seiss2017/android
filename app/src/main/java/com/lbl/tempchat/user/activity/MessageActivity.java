package com.lbl.tempchat.user.activity;

import android.graphics.Color;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.user.adapter.MessagePaperAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageActivity extends BaseActivity {

    private MessagePaperAdapter messagePaperAdapter;

    @BindView(R.id.tab_message)
    TabLayout tab_message;

    @BindView(R.id.vp_message)
    ViewPager vp_message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }

    @Override
    protected void initVariables(){
        messagePaperAdapter=new MessagePaperAdapter(getSupportFragmentManager());
    }

    @Override
    protected void initViews(){
        //设置标题栏
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_message);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("我的消息");
        actionBar.setDisplayHomeAsUpEnabled(true);

        vp_message.setAdapter(messagePaperAdapter);
        tab_message.setupWithViewPager(vp_message);

        vp_message.setOffscreenPageLimit(2);
    }

    @Override
    protected void loadData(){}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }
}
