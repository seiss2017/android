package com.lbl.tempchat.chat.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.chat.adapter.ChatListAdapter;
import com.lbl.tempchat.chat.entity.ChatListMessage;
import com.lbl.tempchat.common.PushService;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.SocketTypeCode;
import com.lbl.tempchat.common.request.entity.ChatInfo;
import com.lbl.tempchat.common.request.entity.ChatMessage;
import com.lbl.tempchat.common.request.entity.ChatSendMessage;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.GsonUtil;
import com.lbl.tempchat.utils.MyDateUtils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class ChatActivity extends BaseActivity {

    public static final String CHAT_INFO="chat_info";

    public static final String RECEIVER_MESSAGE="receiver_message";

    private OkHttpClient httpClient;

    private RequestBase<UserInfo> requestUserInfo;

    @BindView(R.id.toolbar_chat)
    Toolbar toolbar;


    @BindView(R.id.lv_chat)
    ListView lv_chat;

    @BindView(R.id.et_chat)
    TextInputEditText et_chat;

    private ChatInfo chatInfo;

    private ChatListAdapter chatListAdapter;

    //private boolean isSendFinish=true;

    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_USER_INFO_SUCCESS:
                    chatListAdapter.setSender(requestUserInfo.getStatus().getData());
                    chatListAdapter.notifyDataSetChanged();
                    break;
                case RequestCode.GET_USER_INFO_FAILURE:
                    Toast.makeText(ChatActivity.this,"获取用户信息失败",Toast.LENGTH_SHORT).show();
                    break;

                case RequestCode.SEND_CHAT_MESSAGE_SUCCESS:
                    handleSendSuccess();
                    break;
                case RequestCode.SEND_CHAT_MESSAGE_FAILURE:
                    //Toast.makeText(ChatActivity.this,"发送失败,请稍后再试",Toast.LENGTH_SHORT).show();
                    //isSendFinish=true;
                    break;
                case RequestCode.GET_CHAT_RECEIVER_MESSAGE_SUCCESS:
                    handleReceiverMessage((ChatMessage) msg.getData().getSerializable(RECEIVER_MESSAGE));
                    break;
                case RequestCode.SEND_ALIVE_MESSAGE:
                    handleAlive();
                    break;
                case RequestCode.FINISH_CHAT:
                    handleFinishChat();
                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();


    }


    @Override
    protected void onResume() {
        super.onResume();
        PushService.setChatHandler(handler);
    }

    @Override
    protected void initVariables(){
        Intent intent=getIntent();
        chatInfo=(ChatInfo)intent.getSerializableExtra(CHAT_INFO);
        chatListAdapter=new ChatListAdapter(this);
        requestUserInfo=new RequestBase<>(String.format(getString(R.string.url_user_info_get), User.getUser().getUserToken(this)),
                RequestCode.GET_USER_INFO_SUCCESS,
                RequestCode.GET_USER_INFO_FAILURE);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        setSupportActionBar(toolbar);
        //设置返回键
        ActionBar actionBar=this.getSupportActionBar();
        if(chatInfo.getUser().getId()==0){
            actionBar.setTitle(chatInfo.getUser().getNickname().replace("小浪","小汪"));
        }else {
            actionBar.setTitle(chatInfo.getUser().getNickname());
        }

        actionBar.setDisplayHomeAsUpEnabled(true);

        lv_chat.setAdapter(chatListAdapter);
    }



    @Override
    protected void loadData(){
        requestUserInfo.requestGet(this,handler,httpClient);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                handleBack();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }

    @OnClick(R.id.btn_chat_send)
    protected void send(){
       /* if(!isSendFinish){
            Toast.makeText(this,"还未发送完成,请稍等",Toast.LENGTH_SHORT).show();
            return;
        }*/
        if(et_chat.length()==0){
            Toast.makeText(this,"内容不能为空",Toast.LENGTH_SHORT).show();
            return;
        }
        //isSendFinish=false;

        String content=et_chat.getText().toString();
        ChatSendMessage chatSendMessage=new ChatSendMessage(SocketTypeCode.SEND_CHAT,chatInfo.getSessionId(),content);
        if(PushService.send(chatSendMessage)){
            ChatListMessage chatListMessage=new ChatListMessage(ChatListMessage.USER_SEND,content,new Date().getTime());
            chatListAdapter.setWaitToAdd(chatListMessage);
        }else {
            //isSendFinish=true;
            Toast.makeText(this,"网络不好,请稍后再试",Toast.LENGTH_SHORT).show();
        }
    }

    private void handleSendSuccess(){
        et_chat.setText("");
        chatListAdapter.addWaitToList();
        chatListAdapter.notifyDataSetChanged();
        lv_chat.setSelection(chatListAdapter.getCount()-1);
        //isSendFinish=true;
    }

    private void handleReceiverMessage(ChatMessage chatMessage){
        ChatListMessage chatListMessage=new ChatListMessage(ChatListMessage.USER_RECEIVER,chatMessage.getContent(),
                MyDateUtils.formatStringToDate(chatMessage.getSendTime()).getTime());
        chatListMessage.setUserInfo(chatMessage.getFrom());
        chatListAdapter.add(chatListMessage);
        chatListAdapter.notifyDataSetChanged();
        lv_chat.setSelection(chatListAdapter.getCount()-1);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            handleBack();
            return false;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }

    private void handleBack(){
        new AlertDialog.Builder(this)
                .setTitle("确定要退出聊天?")
                .setMessage("你是不是看不起我们的匹配算法!竟然觉得匹配对象不合适!")
                .setNegativeButton("取消",null)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ChatSendMessage chatSendMessage=new ChatSendMessage(SocketTypeCode.STOP_CHAT,chatInfo.getSessionId(),null);
                        PushService.send(chatSendMessage);
                        if(!ChatActivity.this.isFinishing()){
                            ChatActivity.this.finish();
                        }
                    }
                })
                .create()
                .show();
    }

    private void handleAlive(){
        if(this.isFinishing()){
            return;
        }
        new AlertDialog.Builder(this)
                .setTitle("是否继续聊天?")
                .setMessage("聊天时间已到,如果要继续聊天,点击确认,否则点击取消")
                .setNegativeButton("取消",new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!ChatActivity.this.isFinishing()){
                            ChatActivity.this.finish();
                        }
                    }
                } )
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ChatSendMessage chatSendMessage=new ChatSendMessage(SocketTypeCode.SEND_ALIVE,chatInfo.getSessionId(),null);
                        PushService.send(chatSendMessage);

                    }
                })
                .create()
                .show();
    }

    public void handleFinishChat(){
        if(this.isFinishing()){
            return;
        }
        new AlertDialog.Builder(this)
                .setTitle("聊天已经结束")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(ChatActivity.this.isFinishing()==false){
                            ChatActivity.this.finish();
                        }

                    }
                })
                .create()
                .show();
    }
}
