package com.lbl.tempchat.common.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.github.chrisbanes.photoview.PhotoView;
import com.lbl.tempchat.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;


public class BigPictureActivity extends BaseActivity {

    public static final String URL="url";

    public static final String RESOURCE_ID="resource_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_picture);
        Intent intent=getIntent();
        String url=intent.getStringExtra(URL);
        int resoureId=intent.getIntExtra(RESOURCE_ID,-1);
        PhotoView pv_big_picture=(PhotoView)findViewById(R.id.pv_big_picture);

        if(url!=null&&url.equals("")==false){
            Picasso.with(this).load(url).memoryPolicy(MemoryPolicy.NO_CACHE).into(pv_big_picture);
        }else if(resoureId!=-1) {
            Picasso.with(this).load(resoureId).memoryPolicy(MemoryPolicy.NO_CACHE).into(pv_big_picture);
        }

        pv_big_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BigPictureActivity.this.finish();
            }
        });
    }


    public static void startThisActivity(Activity activity, String url){
        Intent intent=new Intent();
        intent.putExtra(URL,url);
        intent.setAction(activity.getString(R.string.activity_big_picture));
        activity.startActivity(intent);
    }

    public static void startThisActivity(Activity activity,int resourceId){
        Intent intent=new Intent();
        intent.putExtra(RESOURCE_ID,resourceId);
        intent.setAction(activity.getString(R.string.activity_big_picture));
        activity.startActivity(intent);
    }
}
