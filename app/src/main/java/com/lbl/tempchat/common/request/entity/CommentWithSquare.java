package com.lbl.tempchat.common.request.entity;

/**
 * Created by 13616 on 2017/5/8.
 */

public class CommentWithSquare {
    private int id;

    private String time;

    private UserInfo reviewer;

    private int piazzaId;

    private String content;

    private int likes;

    private SquareMessage source;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public UserInfo getReviewer() {
        return reviewer;
    }

    public void setReviewer(UserInfo reviewer) {
        this.reviewer = reviewer;
    }

    public int getPiazzaId() {
        return piazzaId;
    }

    public void setPiazzaId(int piazzaId) {
        this.piazzaId = piazzaId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }


    public SquareMessage getSource() {
        return source;
    }

    public void setSource(SquareMessage source) {
        this.source = source;
    }
}
