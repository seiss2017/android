package com.lbl.tempchat.common.database.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.lbl.tempchat.common.request.entity.UserInfo;

/**
 * Created by 13616 on 2017/5/8.
 */
@Table(name="squareMessage")
public class SquareMessage extends Model{
    public SquareMessage(){}

    public SquareMessage(com.lbl.tempchat.common.request.entity.SquareMessage squareMessage){
        this.id=squareMessage.getId();
        this.time=squareMessage.getTime();
        this.poster=new UserInfoMessage(squareMessage.getPoster()).save();
        this.content=squareMessage.getContent();
        this.comments=squareMessage.getComments();
        this.likes=squareMessage.getLikes();
        this.hasLiked=squareMessage.isHasLiked();
    }


    @Column(name = "squareId")
    public int id;

    @Column(name="time")
    public String time;

    @Column(name="poster")
    public long poster;

    @Column(name="content")
    public String content;

    @Column(name="comments")
    public int comments;

    @Column(name="likes")
    public int likes;

    @Column(name="hasLiked")
    public boolean hasLiked;
}
