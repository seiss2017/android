package com.lbl.tempchat.user.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.request.entity.Interest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 13616 on 2017/5/5.
 */

public class InterestAdapter extends BaseAdapter {
    private ArrayList<Interest> interestList;

    private Activity activity;

    public InterestAdapter(Activity activity){
        this.activity=activity;
        interestList=new ArrayList<>();
    }

    public void add(ArrayList<Interest> interestList){
        this.interestList.addAll(interestList);
    }

    public void clear(){
        interestList.clear();
    }

    @Override
    public int getCount() {
        return interestList.size();
    }

    @Override
    public Object getItem(int position) {
        return interestList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       ViewHolder viewHolder;
        if(convertView!=null){
            viewHolder=(ViewHolder)convertView.getTag();
        }else {
            convertView= LayoutInflater.from(activity).inflate(R.layout.item_interest, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        viewHolder.tv_item_interest.setText(interestList.get(position).getName());
        return convertView;
    }


    class ViewHolder{
        @BindView(R.id.tv_item_interest)
        TextView tv_item_interest;
        public ViewHolder(View view){
            ButterKnife.bind(this,view);
        }
    }
}
