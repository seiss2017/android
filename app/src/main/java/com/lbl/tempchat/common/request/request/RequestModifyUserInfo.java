package com.lbl.tempchat.common.request.request;

import android.app.Activity;

import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.entity.UserInfo;

import java.util.HashMap;

import okhttp3.OkHttpClient;

/**
 * Created by 13616 on 2017/4/30.
 */

public class RequestModifyUserInfo extends RequestBase<UserInfo> {
    public RequestModifyUserInfo(String url, int successCode, int failureCode){
        super(url,successCode, failureCode);
    }

    @Override
    public void requestPost(Activity activity, RequestHandler handler, OkHttpClient httpClient, HashMap<String,String> map){

    }
}
