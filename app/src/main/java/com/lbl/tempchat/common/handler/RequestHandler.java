package com.lbl.tempchat.common.handler;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.login.activity.LoginActivity;
import com.lbl.tempchat.user.info.User;

import static com.lbl.tempchat.common.request.StatusCode.*;

/**
 * Created by 13616 on 2017/4/28.
 */

public class RequestHandler extends Handler{
    private Activity activity;

    public RequestHandler(Activity activity){
        this.activity=activity;
    }

    public void setActivity(Activity activity){
        this.activity=activity;
    }



    @Override
    public void handleMessage(Message msg){
        if(activity.isFinishing()==true){
            return;
        }
        switch (msg.what){
            case AUTHENTICATION_FAILED:
                if(activity instanceof LoginActivity){
                    Toast.makeText(activity,"账户或密码错误",Toast.LENGTH_SHORT).show();
                    break;
                }
                User.getUser().setUserToken(activity," ");
                User.getUser().login(activity);
                break;
            case ERROR_DATA:
                Toast.makeText(activity,"参数错误",Toast.LENGTH_SHORT).show();
                break;
            case USER_NULL:
                Toast.makeText(activity,"用户名错误",Toast.LENGTH_SHORT).show();
                break;
            case USER_EXISTS:
                Toast.makeText(activity,"用户名已注册",Toast.LENGTH_SHORT).show();
                break;
            case PASSWORD_ERROR:
                Toast.makeText(activity,"密码错误",Toast.LENGTH_SHORT).show();
                break;
            case PASSWORD_LENGTH_ERROR:
                Toast.makeText(activity,"密码太短",Toast.LENGTH_SHORT).show();
                break;
            case FILE_UPLOAD_FAILED:
                Toast.makeText(activity,"文件上传失败",Toast.LENGTH_SHORT).show();
                break;
            case FILE_TOO_LARGE:
                Toast.makeText(activity,"文件过大",Toast.LENGTH_SHORT).show();
                break;
            case UNSUPPORT_IMAGE_FORMAT:
                Toast.makeText(activity,"不支持的图片格式",Toast.LENGTH_SHORT).show();
                break;
            case WRONG_EMAIL:
                Toast.makeText(activity,"邮箱错误",Toast.LENGTH_SHORT).show();
                break;
            case EMAIL_EXISTS:
                Toast.makeText(activity,"邮箱已存在",Toast.LENGTH_SHORT).show();
                break;
            case CHECKER_VALIDATE_FAILED:
                Toast.makeText(activity,"验证码验证失败",Toast.LENGTH_SHORT).show();
                break;
            case CHECKER_EXPIRED:
                Toast.makeText(activity,"验证码过期",Toast.LENGTH_SHORT).show();
                break;
            case SEND_EMAIL_FAILED:
                Toast.makeText(activity,"邮件发送失败",Toast.LENGTH_SHORT).show();
                break;
            case SESSION_NOT_EXIST:
                Toast.makeText(activity,"会话不存在",Toast.LENGTH_SHORT).show();
            case MATCH_TIME_LIMITED:
                Toast.makeText(activity,"匹配次数已经用完",Toast.LENGTH_SHORT).show();
                break;
            case TIME_LIMITED:
                break;
            case SORRY_YOU_ARE_GOOD_GUY:
                break;
            case SESSION_STATUS_ERROR:
                break;

        }
    }

    public boolean handlerErrorMessage(int msg){
        switch (msg){
            case AUTHENTICATION_FAILED:
                break;
            case ERROR_DATA:
                break;
            case USER_NULL:
                break;
            case USER_EXISTS:
                break;
            case PASSWORD_ERROR:
                break;
            case PASSWORD_LENGTH_ERROR:
                break;
            case FILE_UPLOAD_FAILED:
                break;
            case FILE_TOO_LARGE:
                break;
            case UNSUPPORT_IMAGE_FORMAT:
                break;
            case WRONG_EMAIL:
                break;
            case EMAIL_EXISTS:
                break;
            case CHECKER_VALIDATE_FAILED:
                break;
            case CHECKER_EXPIRED:
                break;
            case SEND_EMAIL_FAILED:
                break;
            case SESSION_NOT_EXIST:
                break;
            case MATCH_TIME_LIMITED:
                break;
            case TIME_LIMITED:
                break;
            case SORRY_YOU_ARE_GOOD_GUY:
                break;
            case SESSION_STATUS_ERROR:
                break;
            default:
                return false;

        }
        return sendEmptyMessage(msg);
    }
}
