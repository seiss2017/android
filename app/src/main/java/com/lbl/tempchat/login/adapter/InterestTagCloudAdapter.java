package com.lbl.tempchat.login.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.request.entity.Interest;
import com.moxun.tagcloudlib.view.TagsAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 13616 on 2017/4/11.
 */

public class InterestTagCloudAdapter extends TagsAdapter {

    private Activity activity;

    private ArrayList<Interest> interestList;

    private HashMap<Integer,Boolean> map;

    public InterestTagCloudAdapter(Activity activity){
        this.activity=activity;
        interestList=new ArrayList<>();
        map=new HashMap<>();
    }

    public void add(ArrayList<Interest> interestList){
        this.interestList.addAll(interestList);
    }

    public HashMap<Integer,Boolean> getMap(){
        return map;
    }

    @Override
    public int getCount() {
        return interestList.size();
    }

    @Override
    public View getView(Context context, int position, ViewGroup parent) {
        Interest interest=interestList.get(position);
        TextView tv = new TextView(context);
        tv.setBackground(activity.getDrawable(R.drawable.btn_circleview));
        tv.setText(interest.getName());
        tv.setPadding(16,8,16,8);
        tv.setGravity(Gravity.CENTER);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tempTextView=(TextView)v;
                if(map.containsKey(interest.getId())&&map.get(interest.getId())){
                    tempTextView.setTextColor(Color.WHITE);
                    map.put(interest.getId(),false);
                }else {
                    tempTextView.setTextColor(Color.RED );
                    map.put(interest.getId(),true);
                }

            }
        });
        if(map.containsKey(interest.getId())&&map.get(interest.getId())){
            tv.setTextColor(Color.RED);
        }else {
            tv.setTextColor(Color.WHITE );
        }
        //tv.setTextColor(Color.WHITE);
        return tv;
    }

    @Override
    public Object getItem(int position) {
        return interestList.get(position);
    }

    @Override
    public int getPopularity(int position) {
        return position%7;
    }

    @Override
    public void onThemeColorChanged(View view, int themeColor) {
        //view.setBackgroundColor(themeColor);
    }

}
