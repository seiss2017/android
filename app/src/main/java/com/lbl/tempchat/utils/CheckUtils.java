package com.lbl.tempchat.utils;

import java.util.regex.Pattern;

/**
 * Created by 13616 on 2017/4/28.
 */

public class CheckUtils {
    private static Pattern email=
            Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
    private static Pattern phone= Pattern.compile("^((13[0-9])|(17[8,0,6,7])|(15[^4,\\D])|(18[0-9]))\\d{8}$");

    /**
     * 验证邮箱合法性
     * @param content 内容
     * @return
     */
    public static boolean isEmail(String content){
        return content==null?false:email.matcher(content.trim()).matches();
    }
    /**
     * 验证手机号合法性
     * @param content 内容
     * @return
     */
    public static boolean isPhone(String content){
        return  content==null?false:phone.matcher(content.trim()).matches();
    }

}
