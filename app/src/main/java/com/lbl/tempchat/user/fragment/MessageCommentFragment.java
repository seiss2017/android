package com.lbl.tempchat.user.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Select;
import com.lbl.tempchat.R;
import com.lbl.tempchat.common.PullListView.XListView;
import com.lbl.tempchat.common.PushService;
import com.lbl.tempchat.common.database.entity.CommentMessage;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.user.adapter.MessageCommentAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MessageCommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessageCommentFragment extends Fragment {

    private MessageCommentAdapter messageCommentAdapter;

    private List<CommentMessage> commentMessageList;

    private RequestHandler handler=new RequestHandler(this.getActivity()){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.RECEIVER_COMMENT_NOTIFICATION_SUCCESS:
                    handleReceiverComment();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };



    public MessageCommentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment MessageCommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MessageCommentFragment newInstance() {
        MessageCommentFragment fragment = new MessageCommentFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        messageCommentAdapter=new MessageCommentAdapter(this.getActivity());
        handler.setActivity(this.getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_message_comment, container, false);

        XListView lv_message_comment=(XListView)rootView.findViewById(R.id.lv_message_comment);
        lv_message_comment.setPullLoadEnable(false);
        lv_message_comment.setPullRefreshEnable(false);
        lv_message_comment.setAutoLoadEnable(false);
        lv_message_comment.setAdapter(messageCommentAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        handleReceiverComment();
        PushService.commentHandler=handler;
    }

    public void handleReceiverComment(){
        commentMessageList=new Select().from(CommentMessage.class).where("source!=-1").execute();

        messageCommentAdapter.add(commentMessageList);
        messageCommentAdapter.notifyDataSetChanged();
    }


}
