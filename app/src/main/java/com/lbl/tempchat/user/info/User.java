package com.lbl.tempchat.user.info;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.lbl.tempchat.R;

/**
 * Created by 13616 on 2017/4/28.
 */

public class User {
    public static final String DATA_NO_EXIST=" ";

    private static User user=new User();

    private User(){}

    public static User getUser(){
        if(user!=null){
            return user;
        }else {
            synchronized (User.class){
                if(user==null){
                    user=new User();
                }
                return user;
            }
        }
    }

    public int getUserId(Context context){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        int userId=share.getInt("uid",-1);
        return userId;
    }

    public void setUserId(Context context,int uid){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        SharedPreferences.Editor editor=share.edit();
        editor.putInt("uid",uid);
    }
    public String getUserToken(Context context){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        String token=share.getString("token"," ");
        return token;
    }

    public void setUserToken(Context context,String token){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        SharedPreferences.Editor editor=share.edit();
        editor.putString("token",token);
        editor.commit();
    }

    public String getUserCipher(Context context){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        String cipher=share.getString("cipher"," ");
        return cipher;
    }

    public void setUserCipher(Context context,String cipher){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        SharedPreferences.Editor editor=share.edit();
        editor.putString("cipher",cipher);
        editor.commit();
    }

    public void setUserEmail(Context context,String email){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        SharedPreferences.Editor editor=share.edit();
        editor.putString("email",email);
        editor.commit();
    }

    public String getUserEmail(Context context){
        SharedPreferences share=context.getSharedPreferences(context.getString(R.string.sharepreference_user),context.MODE_PRIVATE);
        String email=share.getString("email"," ");
        return email;
    }
    public boolean isLogin(Context context){
        return getUserToken(context).equals(" ")?false:true;
    }


    public void login(Activity activity){
        Intent intent=new Intent();
        intent.setAction(activity.getString(R.string.activity_login));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }
}
