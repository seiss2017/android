package com.lbl.tempchat.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.Status;
import com.lbl.tempchat.common.request.StatusSocket;
import com.lbl.tempchat.common.request.entity.ChatInfo;
import com.lbl.tempchat.common.request.entity.ChatMessage;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.CommentWithComment;
import com.lbl.tempchat.common.request.entity.CommentWithSquare;
import com.lbl.tempchat.common.request.entity.Interest;
import com.lbl.tempchat.common.request.entity.PageList;
import com.lbl.tempchat.common.request.entity.PraiseMessage;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.common.request.entity.UserInfo;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.lbl.tempchat.common.request.RequestCode.*;

/**
 * Created by 13616 on 2017/4/29.
 */

public class GsonUtil {
    private static GsonUtil gsonUtil;

    private GsonUtil(){
        gson=new GsonBuilder().create();
    }

    public static GsonUtil getGsonUtil(){
        if(gsonUtil!=null){
            return gsonUtil;
        }else {
            synchronized (GsonUtil.class){
                if(gsonUtil==null){
                    gsonUtil=new GsonUtil();
                }
                return gsonUtil;
            }
        }

    }

    private Gson gson;

    public Gson getGson(){
        return gson;
    }

    public static Type getResponseType(int requestCode){
        switch (requestCode){
           /* case RequestCode.GET_CHECKER_SUCCESS:
                return new TypeToken<Status<String>>(){}.getType();
            case RequestCode.LOGIN_SUCCESS:
                return new TypeToken<Status<String>>(){}.getType();
            case RequestCode.REGISTER_SUCCESS:
                return new TypeToken<Status<String>>(){}.getType();*/
            case GET_USER_INFO_SUCCESS:
                return new TypeToken<Status<UserInfo>>(){}.getType();
            case MODIFY_USER_INFO_SUCCESS:
                return new TypeToken<Status<UserInfo>>(){}.getType();
            case GET_SQUARE_MESSAGE_SUCCESS:
                return new TypeToken<Status<PageList<SquareMessage>>>(){}.getType();
            case GET_MOMENT_DETAIL_SUCCESS:
                return new TypeToken<Status<SquareMessage>>(){}.getType();
            case GET_MOMENT_DETAIL_COMMENT_SUCCESS:
                return new TypeToken<Status<PageList<Comment>>>(){}.getType();
            case SEND_MOMENT_DETAIL_COMMENT_SUCCESS:
                return new TypeToken<Status<Comment>>(){}.getType();
            case SEND_MOMENT_SUCCESS:
                return new TypeToken<Status<SquareMessage>>(){}.getType();
            case SQUARE_MESSAGE_PRAISE_SUCCESS:
                return new TypeToken<Status<Integer>>(){}.getType();
            case GET_INTEREST_TYPE_SUCCESS:
                return new TypeToken<Status<ArrayList<String>>>(){}.getType();
            case GET_INTEREST_TYPE_LIST_SUCCESS:
                return new TypeToken<Status<ArrayList<Interest>>>(){}.getType();
            case GET_CHAT_USER_SUCCESS:
                return new TypeToken<Status<ChatInfo>>(){}.getType();
            case GET_WEBSOCKET_INFO:
                return new TypeToken<StatusSocket<JSONObject>>(){}.getType();
            case GET_CHAT_RECEIVER_MESSAGE_SUCCESS:
                return new TypeToken<StatusSocket<ChatMessage>>(){}.getType();
            case SQUARE_MESSAGE_CANCEL_PRAISE_SUCCESS:
                return new TypeToken<Status<Integer>>(){}.getType();
            case GET_USER_INTEREST_SUCCESS:
                return new TypeToken<Status<ArrayList<Interest>>>(){}.getType();
            case GET_ALL_INTEREST_SUCCESS:
                return new TypeToken<Status<ArrayList<Interest>>>(){}.getType();
            case RECEIVER_COMMENT_WITH_COMMENT_NOTIFICATION:
                return new TypeToken<StatusSocket<CommentWithComment>>(){}.getType();
            case RECEIVER_COMMENT_WITH_SQUAREMESSAGE_NOTIFICATION:
                return new TypeToken<StatusSocket<CommentWithSquare>>(){}.getType();
            case RECEIVER_PRAISE_NOTIFICATION_SUCCESS:
                return new TypeToken<StatusSocket<PraiseMessage>>(){}.getType();
            case ACCEPT_CHAT:
                return new TypeToken<StatusSocket<ChatInfo>>(){}.getType();
        }
        return new TypeToken<Status<String>>(){}.getType();
    }




}
