package com.lbl.tempchat.common.request.entity;

import java.util.ArrayList;

/**
 * Created by 13616 on 2017/5/9.
 */

public class PraiseMessage {

    private ArrayList<String> images;

    private int id;

    private String time;

    private UserInfo poster;

    private String content;

    private int comments;

    private int likes;

    private boolean hasLiked;

    private UserInfo liker;


    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public UserInfo getPoster() {
        return poster;
    }

    public void setPoster(UserInfo poster) {
        this.poster = poster;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isHasLiked() {
        return hasLiked;
    }

    public void setHasLiked(boolean hasLiked) {
        this.hasLiked = hasLiked;
    }

    public UserInfo getLiker() {
        return liker;
    }

    public void setLiker(UserInfo liker) {
        this.liker = liker;
    }
}
