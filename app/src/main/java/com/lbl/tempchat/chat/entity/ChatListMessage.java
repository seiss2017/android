package com.lbl.tempchat.chat.entity;

import android.support.annotation.NonNull;

import com.lbl.tempchat.common.request.entity.UserInfo;

/**
 * Created by 13616 on 2017/5/4.
 */

public class ChatListMessage implements Comparable{

    public ChatListMessage(){}
    public ChatListMessage(int type,String content,long time){
        this.type=type;
        this.content=content;
        this.time=time;
    }

    public static final int USER_SEND=0;

    public static final int USER_RECEIVER=1;

    private int type;

    private long time;

    private String content;

    private UserInfo userInfo;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        ChatListMessage other=(ChatListMessage)o;
        return time>other.getTime()?1:-1;
    }

}
