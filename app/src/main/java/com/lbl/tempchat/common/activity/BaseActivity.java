package com.lbl.tempchat.common.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lbl.tempchat.common.PushService;

/**
 * Created by 13616 on 2017/4/6.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
        PushService.nowActivity=this;
    }
    protected void initVariables(){}

    protected void initViews(){}

    protected void loadData(){}

    protected void setViews(){}
}
