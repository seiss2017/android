package com.lbl.tempchat.common.request.request;

import com.google.gson.reflect.TypeToken;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.Status;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.utils.GsonUtil;

/**
 * Created by 13616 on 2017/4/29.
 */

public class RequestUserInfo extends RequestBase<UserInfo> {
    public RequestUserInfo(String url, int successCode, int failureCode){
        super(url,successCode, failureCode);
    }


}
