package com.lbl.tempchat.square.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.adapter.ChoosePhotoListAdapter;
import com.lbl.tempchat.common.adapter.MyGraidViewAdapter;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.listener.ChooseImageListener;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.common.request.request.RequestSendMoment;
import com.lbl.tempchat.common.view.MyGridView;
import com.lbl.tempchat.user.info.User;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;

public class MomentSendActivity extends BaseActivity {



    private ChoosePhotoListAdapter choosePhotoListAdapter;

    private OkHttpClient httpClient;

    private AlertDialog waitingDialog;


    //获取到的照片信息
    private List<PhotoInfo> photoInfoList;

    @BindView(R.id.gv_moment_send_photos)
    MyGridView gv_moment_send_photos;

    @BindView(R.id.tiet_moment_send_input)
    TextInputEditText tiet_moment_send_input;

    private RequestSendMoment requestSendMoment;


    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.SEND_MOMENT_SUCCESS:
                    if(MomentSendActivity.this.isFinishing()==false){
                        if(waitingDialog.isShowing()){
                            waitingDialog.dismiss();
                        }
                        finish();
                    }
                    break;
                case RequestCode.SEND_MOMENT_FAILURE:
                    if(MomentSendActivity.this.isFinishing()==false){
                        if(waitingDialog.isShowing()){
                            waitingDialog.dismiss();
                        }
                        Toast.makeText(MomentSendActivity.this,"发送失败",Toast.LENGTH_SHORT).show();
                    }

                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment_send);

        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }



    @Override
    protected void initVariables(){
        requestSendMoment=new RequestSendMoment(getString(R.string.url_square_send),
                RequestCode.SEND_MOMENT_SUCCESS,
                RequestCode.SEND_MOMENT_FAILURE);

        photoInfoList = new ArrayList<>();
        initImageLoader(this);
        choosePhotoListAdapter=new ChoosePhotoListAdapter(this,photoInfoList);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_moment_send);
        setSupportActionBar(toolbar);
        toolbar.setTitle("发动态");
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        gv_moment_send_photos.setAdapter(choosePhotoListAdapter);

        ImageView iv_moment_send_add_photo=(ImageView)findViewById(R.id.iv_moment_send_add_photo);
        iv_moment_send_add_photo.setOnClickListener(new ChooseImageListener(MomentSendActivity.this,mOnHanlderResultCallback,photoInfoList));

        waitingDialog=new SpotsDialog(this,R.style.Custom);

    }


    @Override
    protected void loadData(){

    }

    @OnClick(R.id.rl_moment_send_input_area)
    protected void getFocusForinputTextArea(){
        tiet_moment_send_input.requestFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            //发送动态按钮
            case R.id.menu_send:
                sendMoment();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }

    //图片选择回调函数
    private GalleryFinal.OnHanlderResultCallback mOnHanlderResultCallback = new GalleryFinal.OnHanlderResultCallback() {
        @Override
        public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
            if (resultList != null) {
                /*if(photoInfoList.size()==0){
                    photoInfoList.addAll(resultList);
                }else{
                    for(PhotoInfo result:resultList){
                        for(int i=0;i<photoInfoList.size();i++){
                            if(result.getPhotoPath().equals(photoInfoList.get(i).getPhotoPath())){
                                break;
                            }
                            if(i==photoInfoList.size()-1){
                                photoInfoList.add(result);
                            }
                        }
                    }
                }
                if(resultList.size()==0){
                    photoInfoList.clear();
                }*/
                photoInfoList.clear();
                if(resultList.size()!=0){
                 photoInfoList.addAll(resultList);
                }


                if(photoInfoList.size()>1){
                    gv_moment_send_photos.setNumColumns(3);
                }else {
                    gv_moment_send_photos.setNumColumns(1);
                }
                choosePhotoListAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onHanlderFailure(int requestCode, String errorMsg) {
            Toast.makeText(MomentSendActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
        }
    };

    private void sendMoment(){

        String content=tiet_moment_send_input.getText().toString();
        if(content==null||content.length()>=140){
            Toast.makeText(this,"动态内容应小于140字且不为空",Toast.LENGTH_SHORT).show();
            return;
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("token", User.getUser().getUserToken(this));
        map.put("content",content);
        map.put("time",String.valueOf(24*60));
        requestSendMoment.requestPostFile(this,handler,httpClient,map,photoInfoList);
        waitingDialog.show();
    }

    private void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
