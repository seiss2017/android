package com.lbl.tempchat.common.request.entity;

import com.lbl.tempchat.common.database.entity.UserInfoMessage;
import com.lbl.tempchat.user.info.User;

import java.io.Serializable;

/**
 * Created by 13616 on 2017/4/29.
 */
public class UserInfo implements Serializable {
    public UserInfo(){}

    public UserInfo(UserInfoMessage userInfo){
        this.id=userInfo.id;
        this.nickname=userInfo.nickname;
        this.sex=userInfo.sex;
        this.intro=userInfo.intro;
        this.avatar=userInfo.avatar;
        this.email=userInfo.email;
    }



    public static final String BOY="g";

    public static final String GIRL="m";

    private int id;

    private String nickname;

    private String sex;

    private String intro;

    private String avatar;

    private String email;


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
