package com.lbl.tempchat.user.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.lbl.tempchat.user.fragment.MessageCommentFragment;
import com.lbl.tempchat.user.fragment.MessagePraiseFragment;

/**
 * Created by 13616 on 2017/4/9.
 */

public class MessagePaperAdapter extends FragmentStatePagerAdapter {

    public MessagePaperAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return MessageCommentFragment.newInstance();
            case 1:
                return MessagePraiseFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "评论";
            case 1:
                return "赞";
        }
        return null;
    }
}
