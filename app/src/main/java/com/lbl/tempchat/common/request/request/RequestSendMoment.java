package com.lbl.tempchat.common.request.request;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;

import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.utils.BitmapUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by 13616 on 2017/5/1.
 */

public class RequestSendMoment extends RequestBase<SquareMessage> {
    public RequestSendMoment(String url, int successCode, int failureCode){
        super(url,successCode, failureCode);
    }

    public void requestPostFile(Activity activity, RequestHandler handler, OkHttpClient httpClient, HashMap<String,String> map, List<PhotoInfo> photoInfoList){

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for(PhotoInfo p:photoInfoList){
            File file = new File(p.getPhotoPath());
            Bitmap bm = BitmapUtils.getimage(p.getPhotoPath());
            file=BitmapUtils.saveMyBitmap(bm,file.getName());
            RequestBody requestBody = FormBody.create(MediaType.parse("image/jpeg"), file);
            builder.addFormDataPart("images",file.getName(),requestBody);
        }
        for(Map.Entry<String,String> entry:map.entrySet()){
            builder.addFormDataPart(entry.getKey(),entry.getValue());
        }

        RequestBody requestBody=builder.build();
        Request request=new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        request(handler,httpClient,request);
    }
}
