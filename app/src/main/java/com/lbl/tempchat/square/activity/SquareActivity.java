package com.lbl.tempchat.square.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.PullListView.XListView;
import com.lbl.tempchat.common.PushService;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestBasePage;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.PageList;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.common.request.request.RequestUserInfo;
import com.lbl.tempchat.square.adapter.MomentAdapter;
import com.lbl.tempchat.user.info.User;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

public class SquareActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.lv_moment)
    XListView lv_moment;

    @BindView(R.id.nav_view)
    NavigationView nav_view;
    //@BindView(R.id.iv_user_head)
    CircleImageView iv_user_head;

    //@BindView(R.id.tv_user_nickname)
    TextView tv_user_nickname;

    TextView tv_user_intro;

    private MomentAdapter momentAdapter;

    private OkHttpClient httpClient;

    private RequestBase<UserInfo> requestUserInfo;

    private RequestBasePage<PageList<SquareMessage>> requestSquare;

    private boolean isFinishRequest=true;

    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_USER_INFO_SUCCESS:
                    setUserInfoView();
                    Log.i("lbl","myToken:"+User.getUser().getUserToken(SquareActivity.this));
                    break;
                case RequestCode.GET_USER_INFO_FAILURE:
                    Toast.makeText(SquareActivity.this,"获取用户信息失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.GET_SQUARE_MESSAGE_SUCCESS:
                    handleSquareMessage();
                    break;
                case RequestCode.GET_SQUARE_MESSAGE_FAILURE:
                    Toast.makeText(SquareActivity.this,"获取动态失败",Toast.LENGTH_SHORT).show();
                    endRefresh();
                    lv_moment.setPullLoadEnable(false);
                    lv_moment.setAutoLoadEnable(false);
                    break;
                default:
                    super.handleMessage(msg);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //更新个人数据
        requestUserInfo.requestGet(this,handler,httpClient);
    }

    @Override
    protected void initVariables(){
        requestUserInfo=new RequestBase<>(String.format(getString(R.string.url_user_info_get), User.getUser().getUserToken(this)),
                RequestCode.GET_USER_INFO_SUCCESS,
                RequestCode.GET_USER_INFO_FAILURE);
        requestSquare=new RequestBasePage<>(getString(R.string.url_square),
                RequestCode.GET_SQUARE_MESSAGE_SUCCESS,
                RequestCode.GET_SQUARE_MESSAGE_FAILURE);
        requestSquare.setRow(10);
        momentAdapter =new MomentAdapter(this);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();

    }

    @Override
    protected void initViews(){

        iv_user_head=(CircleImageView)nav_view.getHeaderView(0).findViewById(R.id.iv_user_head);
        tv_user_nickname=(TextView)nav_view.getHeaderView(0).findViewById(R.id.tv_user_nickname);
        tv_user_intro=(TextView)nav_view.getHeaderView(0).findViewById(R.id.tv_user_intro);
        lv_moment.setAutoLoadEnable(true);
        lv_moment.setPullLoadEnable(true);
        lv_moment.setPullRefreshEnable(true);
        lv_moment.setRefreshTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));

        lv_moment.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {
                if(isFinishRequest==false){
                    return;
                }
                isFinishRequest=false;
                momentAdapter.clear();
                requestSquare.setPage(1);
                requestSquare.requestGet(SquareActivity.this,handler,httpClient);

            }

            @Override
            public void onLoadMore() {
                if(isFinishRequest==false){
                    return;
                }
                isFinishRequest=false;
                requestSquare.requestGet(SquareActivity.this,handler,httpClient);
            }
        });
        lv_moment.setAdapter(momentAdapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

            //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        nav_view.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void loadData(){
        if(User.getUser().isLogin(this)){
            Intent intent=new Intent(this,PushService.class);
            startService(intent);
        }
        requestSquare.requestGet(this,handler,httpClient);
        isFinishRequest=false;
        //requestUserInfo.requestGet(this,handler,httpClient);
    }


    private void setUserInfoView(){
        if(requestUserInfo.getStatus().isResult()==false){
            return;
        }
        if(requestUserInfo.getStatus().getData().getAvatar()!=null
                &&!requestUserInfo.getStatus().getData().getAvatar().equals("")){
            Picasso.with(SquareActivity.this).load(requestUserInfo.getStatus().getData().getAvatar())
                    .into(iv_user_head);
        }
        tv_user_nickname.setText(requestUserInfo.getStatus().getData().getNickname());
        tv_user_intro.setText(requestUserInfo.getStatus().getData().getIntro());
        //保存用户的email
        User.getUser().setUserEmail(this,requestUserInfo.getStatus().getData().getEmail());

    }

    @OnClick(R.id.fab_moment_send)
    public void sendMoment(){
        Intent intent=new Intent();
        intent.setAction(getString(R.string.activity_moment_send));
        startActivity(intent);
    }

    @OnItemClick(R.id.lv_moment)
    protected void jumpToMomentDetail(AdapterView<?> parent, View view, int position, long id){
        SquareMessage squareMessage=(SquareMessage)lv_moment.getAdapter().getItem(position);
        Intent intent=new Intent();
        intent.setAction(getString(R.string.activity_moment_detail));
        intent.putExtra(MomentDetailActivity.MOMENT_ID,squareMessage.getId());
        startActivity(intent);
    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.square, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_chat) {
            Intent intent=new Intent();
            intent.setAction(getString(R.string.activity_matchsuccess));
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent=new Intent();
        if (id == R.id.nav_user_info) {
            intent.setAction(getString(R.string.activity_userinfo));
            startActivity(intent);
        } else if (id == R.id.nav_change_password) {
            intent.setAction(getString(R.string.activity_change_password));
            startActivity(intent);
        } else if (id == R.id.nav_message) {
            intent.setAction(getString(R.string.activity_message));
            startActivity(intent);
        } else if (id == R.id.nav_setting) {

        }else if (id == R.id.nav_interest) {
            intent.setAction(getString(R.string.activity_user_interest));
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void handleSquareMessage(){
        PageList<SquareMessage> list=requestSquare.getStatus().getData();
        if(list.getList().size()<list.getPageSize()){
            requestSquare.setPage(1);
            lv_moment.setPullLoadEnable(false);
            lv_moment.setAutoLoadEnable(false);
        }else {
            lv_moment.setPullLoadEnable(true);
            lv_moment.setAutoLoadEnable(true);
            requestSquare.setPage(requestSquare.getPage()+1);
        }
        momentAdapter.add(list.getList());
        momentAdapter.notifyDataSetChanged();
        endRefresh();
    }

    //停止更新的动画
    private void endRefresh(){
        isFinishRequest=true;
        lv_moment.stopRefresh();
        lv_moment.stopLoadMore();
        lv_moment.setRefreshTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
    }

}
