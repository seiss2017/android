package com.lbl.tempchat.login.activity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.CheckUtils;


import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.Request;



public class RegisterActivity extends BaseActivity {

    private RequestBase<String> requestChecker;

    private RequestBase<String> requestRegister;
    private OkHttpClient httpClient;

    @BindView(R.id.tiet_register_check)
    TextInputEditText tiet_register_check;

    @BindView(R.id.tiet_register_account)
    TextInputEditText tiet_register_account;

    @BindView(R.id.tiet_register_password)
    TextInputEditText tiet_register_password;

    @BindView(R.id.tiet_register_password_again)
    TextInputEditText tiet_register_password_again;

    @BindView(R.id.btn_register_request_check)
    Button btn_register_request_check;

    private static final int CHECKER_TIME_DOWN=1000;

    private static final int CHECKER_CAN_GET=1001;


    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_CHECKER_SUCCESS:
                    User.getUser().setUserCipher(RegisterActivity.this,requestChecker.getStatus().getData());
                    break;
                case RequestCode.GET_CHECKER_FAILURE:
                    Toast.makeText(RegisterActivity.this,"获取验证码失败,请稍后重试",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.REGISTER_SUCCESS:
                    new AlertDialog.Builder(RegisterActivity.this)
                            .setTitle("注册成功")
                            .setPositiveButton("确定", (a,b)->{a.dismiss();RegisterActivity.this.finish();})
                            .setCancelable(false)
                            .create()
                            .show();
                    break;
                case RequestCode.REGISTER_FAILURE:
                    Toast.makeText(RegisterActivity.this,"注册错误,请稍后重试",Toast.LENGTH_SHORT).show();
                    break;
                case CHECKER_TIME_DOWN:
                    btn_register_request_check.setText("验证码("+msg.arg1+"s)");
                    break;
                case CHECKER_CAN_GET:
                    btn_register_request_check.setEnabled(true);
                    btn_register_request_check.setText("验证码");
                    btn_register_request_check.setBackground(getDrawable(R.drawable.btn_circleview));
                default:
                    super.handleMessage(msg);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }

    @Override
    protected void initVariables(){
        requestChecker=new RequestBase<>(getString(R.string.url_checker),
                RequestCode.GET_CHECKER_SUCCESS,
                RequestCode.GET_CHECKER_FAILURE);
        requestRegister=new RequestBase<>(getString(R.string.url_register),
                RequestCode.REGISTER_SUCCESS,
                RequestCode.REGISTER_FAILURE);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_register);
        setSupportActionBar(toolbar);
        toolbar.setTitle("邮箱注册");
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void loadData(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }

    @OnClick(R.id.btn_register_request_check)
    public void request_checker(){
        String email=tiet_register_account.getText().toString();
        if(!CheckUtils.isEmail(email)){
            Toast.makeText(this,"请输入正确的邮箱",Toast.LENGTH_SHORT).show();
            Log.i("lbl","email:"+email);
            return;
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("email",email);
        requestChecker.requestPost(this,handler,httpClient,map);
        btn_register_request_check.setEnabled(false);
        btn_register_request_check.setBackground(getDrawable(R.drawable.btn_circleview_grey));
        new Thread(()->{
            for(int i=60;i>=0;i--){
                Message msg=new Message();
                msg.what=CHECKER_TIME_DOWN;
                msg.arg1=i;
                handler.sendMessage(msg);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            handler.sendEmptyMessage(CHECKER_CAN_GET);
        }).start();
    }

    @OnClick(R.id.btn_register_register)
    public void request_register(){
        String email=tiet_register_account.getText().toString();
        if(!CheckUtils.isEmail(email)){
            Toast.makeText(this,"请输入正确的邮箱",Toast.LENGTH_SHORT).show();
            return;
        }
        String password=tiet_register_password.getText().toString();
        String passwordAgain=tiet_register_password_again.getText().toString();
        if(password==null
                ||passwordAgain==null
                ||password.equals("")
                ||passwordAgain.equals("")
                ||password.equals(passwordAgain)==false){
            Toast.makeText(this,"密码不一致",Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.length()<6||password.length()>20){
            Toast.makeText(this,"密码长度应该在6-20之间",Toast.LENGTH_SHORT).show();
        }
        String checker=tiet_register_check.getText().toString();
        if(checker==null||checker.equals("")){
            Toast.makeText(this,"请输入验证码",Toast.LENGTH_SHORT).show();
            return;
        }
        //Log.i("lbl","email:"+email+" passwd:"+password+" checker:"+checker+" cipher:"+User.getUser().getUserToken(this));
        HashMap<String,String> map=new HashMap<>();
        map.put("email",email);
        map.put("passwd",password);
        map.put("checker",checker);
        map.put("cipher",User.getUser().getUserCipher(this));
        requestRegister.requestPost(this,handler,httpClient,map);

    }
}
