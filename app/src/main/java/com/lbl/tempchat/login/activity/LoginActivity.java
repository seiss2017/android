package com.lbl.tempchat.login.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.user.info.User;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class LoginActivity extends BaseActivity {

    private RequestBase<String> requestLogin;


    private OkHttpClient httpClient;

    @BindView(R.id.toolbar_login)
    Toolbar toolbar;

    @BindView(R.id.tiet_account)
    TextInputEditText tiet_account;

    @BindView(R.id.tiet_password)
    TextInputEditText tiet_password;

    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.LOGIN_SUCCESS:
                    User.getUser().setUserToken(LoginActivity.this,requestLogin.getStatus().getData());
                    Log.i("lbl",User.getUser().getUserToken(LoginActivity.this));
                    Intent intent=new Intent();
                    intent.setAction(getString(R.string.activity_square));
                    startActivity(intent);
                    finish();
                    break;
                case RequestCode.LOGIN_FAILURE:
                    Toast.makeText(LoginActivity.this,"登陆失败",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if(User.getUser().isLogin(this)){
            Intent intent=new Intent();
            intent.setAction(getString(R.string.activity_square));
            startActivity(intent);
            finish();
        }

        initVariables();
        initViews();
        loadData();
    }

    @Override
    protected void initVariables(){
        requestLogin=new RequestBase<>(getString(R.string.url_login),
                RequestCode.LOGIN_SUCCESS,
                RequestCode.LOGIN_FAILURE
        );
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
    }

    @OnClick(R.id.btn_login)
    public void login(){
        String account=tiet_account.getText().toString();
        String password=tiet_password.getText().toString();
        if(account==null||account.equals("")){
            Toast.makeText(this,"请输入账户",Toast.LENGTH_SHORT).show();
            return;
        }
        if(password==null||password.equals("")){
            Toast.makeText(this,"请输入密码",Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.length()<6||password.length()>20){
            Toast.makeText(this,"密码长度应该在6-20之间",Toast.LENGTH_SHORT).show();
            return;
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("email",account);
        map.put("passwd",password);
        requestLogin.requestPost(this,handler,httpClient,map);
    }

    @OnClick(R.id.tv_register)
    public void register(){
        Intent intent=new Intent();
        intent.setAction(getString(R.string.activity_register));
        startActivity(intent);
    }

    @OnClick(R.id.tv_forget_password)
    public void forgetPwd(){
        Log.i("lbl","in");
        Intent intent=new Intent();
        intent.setAction(getString(R.string.activity_forget_pwd));
        startActivity(intent);
    }


}
