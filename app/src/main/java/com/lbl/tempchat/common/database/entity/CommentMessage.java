package com.lbl.tempchat.common.database.entity;

import android.support.annotation.NonNull;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.CommentWithComment;
import com.lbl.tempchat.common.request.entity.CommentWithSquare;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.utils.MyDateUtils;

import java.util.Date;

/**
 * Created by 13616 on 2017/5/8.
 */

@Table(name="comment")
public class CommentMessage extends Model implements Comparable{
    public CommentMessage(){}

    public CommentMessage(CommentWithComment comment){
        this.pointTo=new UserInfoMessage(comment.getPointTo()).save();
        this.id=comment.getId();
        this.time=comment.getTime();
        this.reviewer=new UserInfoMessage(comment.getReviewer()).save();
        this.piazzaId=comment.getPiazzaId();
        this.content=comment.getContent();
        this.likes=comment.getLikes();
        this.source=new CommentMessage(comment.getSource()).save();
    }

    public CommentMessage(CommentWithSquare comment){
        this.pointTo=-1;
        this.id=comment.getId();
        this.time=comment.getTime();
        this.reviewer=new UserInfoMessage(comment.getReviewer()).save();
        this.piazzaId=comment.getPiazzaId();
        this.content=comment.getContent();
        this.likes=comment.getLikes();
        this.source=new SquareMessage(comment.getSource()).save();
    }

    public CommentMessage(Comment comment){
        if(comment.getPointTo()==null){
            this.pointTo=-1;
        }else {
            this.pointTo=new UserInfoMessage(comment.getPointTo()).save();
        }

        this.id=comment.getId();
        this.time=comment.getTime();
        this.reviewer=new UserInfoMessage(comment.getReviewer()).save();
        this.piazzaId=comment.getPiazzaId();
        this.content=comment.getContent();
        this.likes=comment.getLikes();
        this.source=-1;
    }

    @Column(name="pointTo")
    public long pointTo;


    @Column(name="CommentId")
    public int id;

    @Column(name="time")
    public String time;

    @Column(name="reviewer")
    public long reviewer;

    @Column(name="piazzaId")
    public int piazzaId;

    @Column(name = "content")
    public String content;

    @Column(name = "likes")
    public int likes;

    @Column(name="source")
    public long source;

    @Override
    public int compareTo(@NonNull Object o) {
        Date date1= MyDateUtils.formatStringToDate(time);
        Date date2= MyDateUtils.formatStringToDate(((CommentMessage)o).time);
        return date1.getTime()>date2.getTime()?-1:1;
    }
}
