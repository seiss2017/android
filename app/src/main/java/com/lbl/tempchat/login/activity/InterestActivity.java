package com.lbl.tempchat.login.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.Interest;
import com.lbl.tempchat.login.adapter.InterestTagCloudAdapter;
import com.lbl.tempchat.user.info.User;
import com.moxun.tagcloudlib.view.TagCloud;
import com.moxun.tagcloudlib.view.TagCloudView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class InterestActivity extends BaseActivity {

    public static final String USER_EXIST_INTEREST="user_exist_interest";

    public ArrayList<Interest> userExistInterestList;

    @BindView(R.id.tcv_interest)
    TagCloudView tcv_interest;

    private InterestTagCloudAdapter interestTagCloudAdapter;

  /*  private RequestBase<ArrayList<String>> requestInterest;

    private ArrayList<RequestBase<ArrayList<Interest>>> requestInterestList;*/
   private RequestBase<ArrayList<Interest>> requestAllInterest;

    private RequestBase<String> requestAddInterest;

    private RequestBase<String> requestDeleteInterest;

    private OkHttpClient httpClient;

    private int finishRequestCount=0;

    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                /*case RequestCode.GET_INTEREST_TYPE_SUCCESS:
                    getInterestList();
                    break;
                case RequestCode.GET_INTEREST_TYPE_FAILURE:
                    Toast.makeText(InterestActivity.this,"获取兴趣列表失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.GET_INTEREST_TYPE_LIST_SUCCESS:
                    for(int i=0;i<requestInterestList.size();i++) {
                        if (requestInterestList.get(i).getStatus() != null
                                && requestInterestList.get(i).getStatus().getData() != null
                                && requestInterestList.get(i).getStatus().getData().size() != 0) {
                            interestTagCloudAdapter.add(requestInterestList.get(i).getStatus().getData());
                            requestInterestList.remove(i);
                            break;
                        }
                    }
                    if(requestInterestList.size()==0){
                        Log.i("lbl","interestsize:"+interestTagCloudAdapter.getCount());
                        interestTagCloudAdapter.notifyDataSetChanged();
                    }
                    break;
                case RequestCode.GET_INTEREST_TYPE_LIST_FAILURE:
                    Toast.makeText(InterestActivity.this,"获取兴趣列表失败",Toast.LENGTH_SHORT).show();
                    break;*/
                case RequestCode.GET_ALL_INTEREST_SUCCESS:
                    interestTagCloudAdapter.add(requestAllInterest.getStatus().getData());
                    if(userExistInterestList!=null){
                        HashSet<Integer> set=new HashSet<>();
                        for(int i=0;i<userExistInterestList.size();i++){
                            set.add(userExistInterestList.get(i).getId());
                        }
                        for(int i=0;i<requestAllInterest.getStatus().getData().size();i++){
                            if(set.contains(requestAllInterest.getStatus().getData().get(i).getId())){
                                interestTagCloudAdapter.getMap().put(requestAllInterest.getStatus().getData().get(i).getId(),true);
                            }
                        }
                    }
                    interestTagCloudAdapter.notifyDataSetChanged();
                    break;
                case RequestCode.GET_ALL_INTEREST_FAILURE:
                    Toast.makeText(InterestActivity.this,"获取兴趣列表失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.ADD_INTEREST_SUCCESS:
                    finishRequestCount++;
                    if(userExistInterestList==null){
                        Intent intent=new Intent();
                        intent.setAction(getString(R.string.activity_square));
                        startActivity(intent);
                        finish();
                    }else if(finishRequestCount==2){
                        finish();
                    }

                    break;
                case RequestCode.ADD_INTEREST_FAILURE:
                    Toast.makeText(InterestActivity.this,"添加兴趣失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.DELETE_USER_INTEREST_SUCCESS:
                    finishRequestCount++;
                    if(finishRequestCount==2){
                        finish();
                    }
                    break;
                case RequestCode.DELETE_USER_INTEREST_FAILURE:
                    Toast.makeText(InterestActivity.this,"删除兴趣失败",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest);
        ButterKnife.bind(this);
        initVariables();
        initViews();
        loadData();

    }

    @Override
    protected void initVariables(){
        interestTagCloudAdapter=new InterestTagCloudAdapter(this);
       /* requestInterest=new RequestBase<>(getString(R.string.url_interest_type),
                RequestCode.GET_INTEREST_TYPE_SUCCESS,
                RequestCode.GET_INTEREST_TYPE_FAILURE);*/
        requestAddInterest=new RequestBase<>(getString(R.string.url_add_interest),
                RequestCode.ADD_INTEREST_SUCCESS,
                RequestCode.ADD_INTEREST_FAILURE);
        requestAllInterest=new RequestBase<>(getString(R.string.url_interest_all),
                RequestCode.GET_ALL_INTEREST_SUCCESS,
                RequestCode.GET_ALL_INTEREST_FAILURE);
        requestDeleteInterest=new RequestBase<>(getString(R.string.url_delete_interest),
                RequestCode.DELETE_USER_INTEREST_SUCCESS,
                RequestCode.DELETE_USER_INTEREST_FAILURE);

        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
        //requestInterestList=new ArrayList<>();

        Intent intent=getIntent();
        userExistInterestList=(ArrayList<Interest>)intent.getSerializableExtra(USER_EXIST_INTEREST);
    }

    @Override
    protected void initViews(){
        tcv_interest.setAdapter(interestTagCloudAdapter);
        //设置标题栏
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_interest);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("请选择兴趣");
    }

    @Override
    protected void loadData(){
        requestAllInterest.requestGet(this,handler,httpClient);
    }

/*    private void getInterestList(){
        for(String name:requestInterest.getStatus().getData()){
            RequestBase<ArrayList<Interest>> request=new RequestBase<>(String.format(getString(R.string.url_interest_type_list),name),
                    RequestCode.GET_INTEREST_TYPE_LIST_SUCCESS,
                    RequestCode.GET_INTEREST_TYPE_LIST_FAILURE);
            requestInterestList.add(request);
            request.requestGet(this,handler,httpClient);
        }
    }*/


    @OnClick(R.id.btn_interest)
    protected void jumpToSquare(){
        //如果没有添加,直接跳转
        if(userExistInterestList==null&&interestTagCloudAdapter.getMap().size()==0){
            handler.sendEmptyMessage(RequestCode.ADD_INTEREST_SUCCESS);
            return;
        }
        {
            HashMap<String, String> map = new HashMap<>();
            map.put("token", User.getUser().getUserToken(this));
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<Integer, Boolean> entry : interestTagCloudAdapter.getMap().entrySet()) {
                if (entry.getValue() == true) {
                    builder.append(entry.getKey() + ",");
                }
            }
            String interestIds = builder.toString();
            map.put("interestIds", interestIds);
            //如果没有添加任何东西,则当做直接完成了
            if(builder.toString().equals("")){
                finishRequestCount++;
            }else {
                requestAddInterest.requestPost(this, handler, httpClient, map);
            }
        }
        if(userExistInterestList!=null){
            HashMap<String,String> map=new HashMap<>() ;
            map.put("token", User.getUser().getUserToken(this));
            StringBuilder builder=new StringBuilder();
            HashSet<Integer> set=new HashSet<>();
            for(int i=0;i<userExistInterestList.size();i++){
                set.add(userExistInterestList.get(i).getId());
            }
            for(Map.Entry<Integer,Boolean> entry:interestTagCloudAdapter.getMap().entrySet()){
                if(entry.getValue()==false&&set.contains(entry.getKey())){
                    builder.append(entry.getKey()+",");

                }
            }
            String interestIds=builder.toString();
            map.put("interestIds",interestIds);
            if(builder.toString().equals("")){
                finishRequestCount++;
            }else {
                requestDeleteInterest.requestPost(this,handler,httpClient,map);
            }
        }

        if(finishRequestCount==2){
            finish();
        }

    }
}
