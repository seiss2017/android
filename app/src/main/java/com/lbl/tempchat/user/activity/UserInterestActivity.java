package com.lbl.tempchat.user.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.os.Environment;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.Interest;
import com.lbl.tempchat.common.request.request.RequestUploadUserHead;
import com.lbl.tempchat.login.activity.InterestActivity;
import com.lbl.tempchat.user.adapter.InterestAdapter;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.ImageUtils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;

public class UserInterestActivity extends BaseActivity {

    @BindView(R.id.toolbar_user_interest)
    Toolbar toolbar;

    @BindView(R.id.lv_user_interest)
    ListView lv_user_interest;

    private InterestAdapter interestAdapter;

    private OkHttpClient httpClient;

    private RequestBase<ArrayList<Interest>> requestUserInterest;

    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_USER_INTEREST_SUCCESS:
                    interestAdapter.clear();
                    interestAdapter.add(requestUserInterest.getStatus().getData());
                    interestAdapter.notifyDataSetChanged();
                    break;
                case RequestCode.GET_USER_INTEREST_FAILURE:
                    Toast.makeText(UserInterestActivity.this,"获取兴趣列表失败",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_interest);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }
    @Override
    protected void onResume(){
        super.onResume();
        requestUserInterest.requestGet(this,handler,httpClient);
    }

    @Override
    protected void initVariables(){
        requestUserInterest=new RequestBase<>(String.format(getString(R.string.url_user_interest),User.getUser().getUserToken(this)),
                RequestCode.GET_USER_INTEREST_SUCCESS,
                RequestCode.GET_USER_INTEREST_FAILURE);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
        interestAdapter=new InterestAdapter(this);
    }

    @Override
    protected void initViews(){
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        //设置返回键
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("个人兴趣列表");
        actionBar.setDisplayHomeAsUpEnabled(true);
        lv_user_interest.setAdapter(interestAdapter);

    }

    @Override
    protected void loadData(){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_interest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_edit_interest:
                editInterest();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }

    private void editInterest(){
        if(requestUserInterest.getStatus()==null){
            return;
        }
        Intent intent=new Intent();
        intent.setAction(getString(R.string.activity_interest));
        intent.putExtra(InterestActivity.USER_EXIST_INTEREST,requestUserInterest.getStatus().getData());
        startActivity(intent);

    }

}
