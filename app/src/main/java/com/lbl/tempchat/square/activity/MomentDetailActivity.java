package com.lbl.tempchat.square.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.PullListView.XListView;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.adapter.MyGraidViewAdapter;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestBasePage;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.PageList;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.common.view.MyGridView;
import com.lbl.tempchat.square.adapter.MomentDetailCommentAdapter;
import com.lbl.tempchat.user.info.User;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

public class MomentDetailActivity extends BaseActivity {

    public static final String MOMENT_ID="momnetId";

    @BindView(R.id.lv_moment_detail_comment)
    XListView lv_moment_detail_comment;

    @BindView(R.id.tiet_moment_detail_comment)
    TextInputEditText tiet_moment_detail_comment;

    CircleImageView iv_moment_head;

    TextView tv_moment_nickname;

    TextView tv_moment_time;

    TextView tv_moment_content;

    TextView tv_moment_praise_times;

    TextView tv_moment_comment_times;

    MyGridView gv_moment_photos;

    ImageView iv_moment_praise;



    private MomentDetailCommentAdapter momentDetailCommentAdapter;

    private MyGraidViewAdapter myGraidViewAdapter;

    private View commentView;

    private RequestBase<SquareMessage> requestMomentDetail;

    private RequestBasePage<PageList<Comment>> requestComment;

    private RequestBase<Integer> requestPraise;


    private RequestBase<Comment> requestSendComment;

    private OkHttpClient httpClient;

    private int momentId;

    private boolean isFinishRequest=true;

    private int commentPointTo=0;

    private int commentPointToId=0;


    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_MOMENT_DETAIL_SUCCESS:
                    handleView();
                    break;
                case RequestCode.GET_MOMENT_DETAIL_FAILURE:
                    Toast.makeText(MomentDetailActivity.this,"获取动态失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.GET_MOMENT_DETAIL_COMMENT_SUCCESS:
                    handleComment();
                    break;
                case RequestCode.GET_MOMENT_DETAIL_COMMENT_FAILURE:
                    endRefresh();
                    Toast.makeText(MomentDetailActivity.this,"获取评论失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.SEND_MOMENT_DETAIL_COMMENT_SUCCESS:
                    commentPointTo=0;
                    tiet_moment_detail_comment.setHint("");
                    tiet_moment_detail_comment.setText("");
                    momentDetailCommentAdapter.addFirst(requestSendComment.getStatus().getData());
                    momentDetailCommentAdapter.notifyDataSetChanged();
                    break;
                case RequestCode.SEND_MOMENT_DETAIL_COMMENT_FAILURE:
                    Toast.makeText(MomentDetailActivity.this,"发送评论失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.SQUARE_MESSAGE_PRAISE_SUCCESS:
                    isFinishRequest=true;
                    iv_moment_praise.setImageResource(R.drawable.moment_praise_pressed);
                    tv_moment_praise_times.setText(String.valueOf(requestPraise.getStatus().getData()));
                    if(requestMomentDetail.getStatus()!=null){
                        requestMomentDetail.getStatus().getData().setHasLiked(true);
                    }
                    break;
                case RequestCode.SQUARE_MESSAGE_PRAISE_FAILURE:
                    isFinishRequest=true;
                    Toast.makeText(MomentDetailActivity.this,"点赞失败",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_SUCCESS:
                    isFinishRequest=true;
                    iv_moment_praise.setImageResource(R.drawable.moment_praise);
                    tv_moment_praise_times.setText(String.valueOf(requestPraise.getStatus().getData()));
                    if(requestMomentDetail.getStatus()!=null){
                        requestMomentDetail.getStatus().getData().setHasLiked(false);
                    }
                    break;
                case RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_FAILURE:
                    isFinishRequest=true;
                    Toast.makeText(MomentDetailActivity.this,"取消点赞失败",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment_detail);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();

    }

    @Override
    protected void initVariables(){
        Intent intent=getIntent();
        momentId=intent.getIntExtra(MOMENT_ID,0);
        requestMomentDetail=new RequestBase<>(String.format(getString(R.string.url_moment_detail),momentId, User.getUser().getUserToken(this)),
                RequestCode.GET_MOMENT_DETAIL_SUCCESS,
                RequestCode.GET_MOMENT_DETAIL_FAILURE);

        requestComment=new RequestBasePage<>(getString(R.string.url_moment_detail_comment).replace("{piazzaId}",String.valueOf(momentId)),
                RequestCode.GET_MOMENT_DETAIL_COMMENT_SUCCESS,
                RequestCode.GET_MOMENT_DETAIL_COMMENT_FAILURE);

        requestSendComment=new RequestBase<>(String.format(getString(R.string.url_moment_detail_comment_send),String.valueOf(momentId)),
                RequestCode.SEND_MOMENT_DETAIL_COMMENT_SUCCESS,
                RequestCode.SEND_MOMENT_DETAIL_COMMENT_FAILURE);


        momentDetailCommentAdapter=new MomentDetailCommentAdapter(this);
        myGraidViewAdapter=new MyGraidViewAdapter(this);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }


    @Override
    protected void initViews(){
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_moment_detail);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        //设置返回键
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("动态");
        actionBar.setDisplayHomeAsUpEnabled(true);
        //设置动态部分
        commentView= LayoutInflater.from(this).inflate(R.layout.item_moment, null);
        iv_moment_head=(CircleImageView)commentView.findViewById(R.id.iv_moment_head);
        tv_moment_nickname=(TextView)commentView.findViewById(R.id.tv_moment_nickname);
        tv_moment_time=(TextView)commentView.findViewById(R.id.tv_moment_time);
        tv_moment_content=(TextView)commentView.findViewById(R.id.tv_moment_content);
        tv_moment_praise_times=(TextView)commentView.findViewById(R.id.tv_moment_praise_times);
        tv_moment_comment_times=(TextView)commentView.findViewById(R.id.tv_moment_comment_times);
        iv_moment_praise=(ImageView)commentView.findViewById(R.id.iv_moment_praise);
        iv_moment_praise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(requestMomentDetail.getStatus()==null){
                    return;
                }
                if(isFinishRequest==false){
                    return;
                }
                isFinishRequest=false;
                if(requestMomentDetail.getStatus().getData().isHasLiked()){
                    requestPraise=new RequestBase<>(String.format(getString(R.string.url_square_message_cancel_praise),momentId),
                            RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_SUCCESS,
                            RequestCode.SQUARE_MESSAGE_CANCEL_PRAISE_FAILURE);
                }else {
                    requestPraise=new RequestBase<>(String.format(getString(R.string.url_square_message_praise),momentId),
                            RequestCode.SQUARE_MESSAGE_PRAISE_SUCCESS,
                            RequestCode.SQUARE_MESSAGE_PRAISE_FAILURE);
                }
                HashMap<String,String> map=new HashMap<>();
                map.put("token",User.getUser().getUserToken(MomentDetailActivity.this));
                requestPraise.requestPost(MomentDetailActivity.this,handler,httpClient,map);
            }
        });

        gv_moment_photos=(MyGridView)commentView.findViewById(R.id.gv_moment_photos);
        gv_moment_photos.setAdapter(myGraidViewAdapter);

        //设置评论列表部分
        lv_moment_detail_comment.setPullRefreshEnable(false);
        lv_moment_detail_comment.setPullLoadEnable(true);
        lv_moment_detail_comment.setAutoLoadEnable(true);
        lv_moment_detail_comment.setRefreshTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));


        lv_moment_detail_comment.addHeaderView(commentView,null,false);
        lv_moment_detail_comment.setAdapter(momentDetailCommentAdapter);

        lv_moment_detail_comment.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
                if(isFinishRequest==false){
                    return;
                }
                isFinishRequest=false;
                requestComment.requestGet(MomentDetailActivity.this,handler,httpClient);

            }
        });

    }

    @Override
    protected void loadData(){
        requestMomentDetail.requestGet(this,handler,httpClient);
        requestComment.requestGet(this,handler,httpClient);
        isFinishRequest=false;
    }


    private void handleView(){
        SquareMessage squareMessage=requestMomentDetail.getStatus().getData();
        if(squareMessage==null){
            return;
        }
        if(squareMessage.getPoster().getAvatar()!=null
                &&!squareMessage.getPoster().getAvatar().equals("")){
            Picasso.with(this).load(squareMessage.getPoster().getAvatar()).into(iv_moment_head);
        }
        tv_moment_nickname.setText(squareMessage.getPoster().getNickname());
        tv_moment_content.setText(squareMessage.getContent());
        tv_moment_time.setText(squareMessage.getTime().replace("T"," "));
        tv_moment_praise_times.setText(String.valueOf(squareMessage.getLikes()));
        tv_moment_comment_times.setText(String.valueOf(squareMessage.getComments()));
        myGraidViewAdapter.add(squareMessage.getImages());
        if(myGraidViewAdapter.getCount()>1){
            gv_moment_photos.setNumColumns(3);
        }else {
            gv_moment_photos.setNumColumns(1);
        }
        myGraidViewAdapter.notifyDataSetChanged();
        if(squareMessage.isHasLiked()){
            iv_moment_praise.setImageResource(R.drawable.moment_praise_pressed);
        }else {
            iv_moment_praise.setImageResource(R.drawable.moment_praise);
        }

    }

    @OnItemClick(R.id.lv_moment_detail_comment)
    protected void replyComment(AdapterView<?> parent, View view, int position, long id){
        Comment comment=(Comment)lv_moment_detail_comment.getAdapter().getItem(position);
        commentPointTo=comment.getReviewer().getId();
        commentPointToId=comment.getId();
        tiet_moment_detail_comment.setHint("回复 "+comment.getReviewer().getNickname()+":");
        tiet_moment_detail_comment.requestFocus();
    }

    @OnClick(R.id.btn_moment_detail_comment_send)
    protected void reply(){
        String content=tiet_moment_detail_comment.getText().toString();
        if(content==null||content.length()==0||content.length()>140){
            Toast.makeText(this,"评论字数应该在140字以内且不为空",Toast.LENGTH_SHORT).show();
            return;
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("piazzaId",String.valueOf(momentId));
        map.put("content",content);
        map.put("pointTo",String.valueOf(commentPointTo));
        map.put("token",User.getUser().getUserToken(this));
        map.put("commentId",String.valueOf(commentPointToId));
        requestSendComment.requestPost(this,handler,httpClient,map);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }


    private void handleComment(){
        PageList<Comment> list=requestComment.getStatus().getData();
        if(list==null){
            return;
        }
        if(list.getList().size()<list.getPageSize()){
            requestComment.setPage(1);
            lv_moment_detail_comment.setPullLoadEnable(false);
            lv_moment_detail_comment.setAutoLoadEnable(false);
        }else {
            lv_moment_detail_comment.setPullLoadEnable(true);
            lv_moment_detail_comment.setAutoLoadEnable(true);
            requestComment.setPage(requestComment.getPage()+1);
        }
        momentDetailCommentAdapter.add(list.getList());
        momentDetailCommentAdapter.notifyDataSetChanged();
        endRefresh();
    }
    //停止更新的动画
    private void endRefresh(){
        isFinishRequest=true;
        lv_moment_detail_comment.stopRefresh();
        lv_moment_detail_comment.stopLoadMore();
        lv_moment_detail_comment.setRefreshTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
    }
}
