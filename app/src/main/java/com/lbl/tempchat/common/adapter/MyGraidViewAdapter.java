package com.lbl.tempchat.common.adapter;

import android.app.Activity;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BigPictureActivity;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.finalteam.toolsfinal.DeviceUtils;

/**
 * Created by 13616 on 2017/4/8.
 */

public class MyGraidViewAdapter extends BaseAdapter {
    private ArrayList<String> paths;

    private Activity activity;

    private int screenWidth;

    public MyGraidViewAdapter(Activity activity){
        this.activity=activity;
        paths=new ArrayList<String>();
        this.screenWidth = DeviceUtils.getScreenPix(activity).widthPixels;
    }

    public void add(ArrayList<String> paths){
        for(String path:paths){
            if(path!=null&&!path.equals("")){
                this.paths.add(path);
            }
        }
    }
    @Override
    public int getCount() {
        return paths.size();
    }

    @Override
    public Object getItem(int position) {
        return paths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView view= (ImageView) LayoutInflater.from(activity).inflate(R.layout.adapter_photo_list_item, null);
        setHeight(view);
        Picasso.with(activity).load(paths.get(position)).memoryPolicy(MemoryPolicy.NO_CACHE).into(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BigPictureActivity.startThisActivity(activity,paths.get(position));
            }
        });
        //TODO:查看大图
        return view;

    }

    private void setHeight(final View convertView) {
        if(paths.size()==1){
            int height = activity.getResources().getDimensionPixelOffset(R.dimen.dynamic_photo_one_height);
            convertView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
            return;
        }
        int height = screenWidth / 3 - 8;
        convertView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
    }

}
