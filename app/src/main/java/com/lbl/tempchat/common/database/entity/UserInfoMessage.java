package com.lbl.tempchat.common.database.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.user.info.User;

/**
 * Created by 13616 on 2017/5/8.
 */
@Table(name="userInfo")
public class UserInfoMessage extends Model {

    public UserInfoMessage(){}

    public UserInfoMessage(UserInfo userInfo){
        this.id=userInfo.getId();
        this.nickname=userInfo.getNickname();
        this.sex=userInfo.getSex();
        this.intro=userInfo.getIntro();
        this.avatar=userInfo.getAvatar();
        this.email=userInfo.getEmail();
    }

    @Column(name = "uid")
    public int id;

    @Column(name="nickname")
    public String nickname;

    @Column(name = "sex")
    public String sex;

    @Column(name="intro")
    public String intro;

    @Column(name ="avator")
    public String avatar;

    @Column(name = "email")
    public String email;

}
