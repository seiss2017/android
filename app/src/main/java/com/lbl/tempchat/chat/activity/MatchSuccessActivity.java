package com.lbl.tempchat.chat.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Message;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.PushService;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.common.request.SocketTypeCode;
import com.lbl.tempchat.common.request.entity.ChatInfo;
import com.lbl.tempchat.common.request.entity.ChatMessage;
import com.lbl.tempchat.common.request.entity.ChatSendMessage;
import com.lbl.tempchat.user.info.User;
import com.lbl.tempchat.utils.GsonUtil;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;

public class MatchSuccessActivity extends BaseActivity {

    @BindView(R.id.iv_match_success_user_head)
    CircleImageView iv_match_success_user_head;

    @BindView(R.id.tv_match_success_user_nickname)
    TextView tv_match_success_user_nickname;


    private RequestBase<ChatInfo> requestChatUser;

    private OkHttpClient httpClient;

    private boolean isFinishRequest=true;

    private AlertDialog waitingDialog;

    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_CHAT_USER_SUCCESS:
                    isFinishRequest=true;
                    updateUI();
                    break;
                case RequestCode.GET_CHAT_USER_FAILURE:
                    isFinishRequest=true;
                    Toast.makeText(MatchSuccessActivity.this,"匹配失败,请稍后再试",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.TARGET_ACCEPT_CHAT:
                    handleAcceptChat();
                    break;
                case RequestCode.TARGET_REJECT_CHAT:
                    handleRejectChat();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_success);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }


    @Override
    protected void onResume() {
        super.onResume();
        PushService.matchHandler=handler;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(waitingDialog.isShowing()){
            waitingDialog.cancel();
        }
    }

    @Override
    protected void initVariables(){
        requestChatUser=new RequestBase<>(String.format(getString(R.string.url_chat_get_user), User.getUser().getUserToken(this)),
                RequestCode.GET_CHAT_USER_SUCCESS,
                RequestCode.GET_CHAT_USER_FAILURE);

        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        waitingDialog=new SpotsDialog(this,R.style.Chat);
    }

    @Override
    protected void loadData(){
        match();
    }


    private void match(){
        if(!isFinishRequest){
            return;
        }

        isFinishRequest=false;
        requestChatUser.requestGet(this,handler,httpClient);
    }

    private void updateUI(){
        /**/
        if(requestChatUser.getStatus().getData().getUser().getId()==0){
            tv_match_success_user_nickname.setText(requestChatUser.getStatus().getData().getUser().getNickname().replace("小浪","小汪"));
        }else {
            tv_match_success_user_nickname.setText(requestChatUser.getStatus().getData().getUser().getNickname());
        }
        if(requestChatUser.getStatus().getData().getUser().getAvatar()!=null
                &&!requestChatUser.getStatus().getData().getUser().getAvatar().equals("")){
            Picasso.with(this).load(requestChatUser.getStatus().getData().getUser().getAvatar()).into(iv_match_success_user_head);
        }
    }


    @OnClick(R.id.btn_match_success_chat)
    public void startChat(){
        if(isFinishRequest==false){
            return;
        }
        if(requestChatUser.getStatus().getData().getUser().getId()==0){
            Intent intent=new Intent();
            intent.setAction(getString(R.string.activity_chat));
            intent.putExtra(ChatActivity.CHAT_INFO,requestChatUser.getStatus().getData());
            startActivity(intent);
            finish();
        }else {
            ChatSendMessage chatSendMessage=new ChatSendMessage(SocketTypeCode.REQUEST_CHAT,requestChatUser.getStatus().getData().getSessionId(),null);
            if(!PushService.send(chatSendMessage)){
                Toast.makeText(this,"网络不好,请稍后再试",Toast.LENGTH_SHORT).show();
            }

            if(!waitingDialog.isShowing()){
                waitingDialog.show();
            }
        }
    }

    @OnClick(R.id.btn_match_success_match_again)
    public void leave(){
        match();
    }



    public void handleAcceptChat(){
        if(waitingDialog.isShowing()){
            waitingDialog.cancel();
        }
        Intent intent=new Intent();
        intent.setAction(getString(R.string.activity_chat));
        intent.putExtra(ChatActivity.CHAT_INFO,requestChatUser.getStatus().getData());
        startActivity(intent);

        finish();
    }
    public void handleRejectChat(){
        if(isFinishing()){
            return;
        }
        if(waitingDialog.isShowing()){
            waitingDialog.cancel();
        }
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("对方拒绝聊天")
                .setPositiveButton("确定", null)
                .create()
                .show();
    }
}
