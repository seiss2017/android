package com.lbl.tempchat.common.request.entity;

import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import com.activeandroid.query.Select;
import com.lbl.tempchat.common.database.entity.PraiseMessage;
import com.lbl.tempchat.common.database.entity.UserInfoMessage;
import com.lbl.tempchat.utils.MyDateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 13616 on 2017/4/30.
 */

public class SquareMessage implements Comparable {
    public SquareMessage(){}

    public SquareMessage(PraiseMessage praiseMessage){
        this.images=praiseMessage.images;
        this.id=praiseMessage.id;
        this.time=praiseMessage.time;
        List<UserInfoMessage> uim=new Select().from(UserInfoMessage.class).where("id=?",praiseMessage.poster).execute();
        this.poster=uim.size()==0?null:new UserInfo(uim.get(0));
        this.content=praiseMessage.content;
        this.comments=praiseMessage.comments;
        this.likes=praiseMessage.likes;
        this.hasLiked=praiseMessage.hasLiked;
    }


    private ArrayList<String> images;

    private int id;

    private String time;

    private UserInfo poster;

    private String content;

    private int comments;

    private int likes;

    private boolean hasLiked;

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public UserInfo getPoster() {
        return poster;
    }

    public void setPoster(UserInfo poster) {
        this.poster = poster;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }



    @Override
    public int compareTo(@NonNull Object o) {
        Date date1= MyDateUtils.formatStringToDate(time);
        Date date2= MyDateUtils.formatStringToDate(((SquareMessage)o).time);
        return date1.getTime()>date2.getTime()?-1:1;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }


    public boolean isHasLiked() {
        return hasLiked;
    }

    public void setHasLiked(boolean hasLiked) {
        this.hasLiked = hasLiked;
    }
}
