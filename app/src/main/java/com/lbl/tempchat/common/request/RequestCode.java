package com.lbl.tempchat.common.request;

/**
 * Created by 13616 on 2017/4/28.
 */

public class RequestCode {
    public final static int GET_CHECKER_SUCCESS=10000;

    public final static int GET_CHECKER_FAILURE=10001;

    public final static int REGISTER_SUCCESS=10002;

    public final static int REGISTER_FAILURE=10003;

    public final static int LOGIN_SUCCESS=10004;

    public final static int LOGIN_FAILURE=10005;

    public final static int GET_USER_INFO_SUCCESS=10006;

    public final static int GET_USER_INFO_FAILURE=10007;

    public final static int UPLOAD_USER_AVATAR_SUCCESS=10008;

    public final static int UPLOAD_USER_AVATAR_FAILURE=10009;

    public final static int MODIFY_USER_INFO_SUCCESS=10010;

    public final static int MODIFY_USER_INFO_FAILURE=10011;

    public final static int GET_SQUARE_MESSAGE_SUCCESS=10012;

    public final static int GET_SQUARE_MESSAGE_FAILURE=10013;

    public final static int GET_MOMENT_DETAIL_SUCCESS=10014;

    public final static int GET_MOMENT_DETAIL_FAILURE=10015;

    public final static int GET_MOMENT_DETAIL_COMMENT_SUCCESS=10016;

    public final static int GET_MOMENT_DETAIL_COMMENT_FAILURE=10017;

    public final static int SEND_MOMENT_DETAIL_COMMENT_SUCCESS=10018;

    public final static int SEND_MOMENT_DETAIL_COMMENT_FAILURE=10019;

    public final static int SEND_MOMENT_SUCCESS=10020;

    public final static int SEND_MOMENT_FAILURE=10021;

    public final static int CHANGE_PASSWORD_SUCCESS=10022;

    public final static int CHANGE_PASSWORD_FAILURE=10023;

    public final static int SQUARE_MESSAGE_PRAISE_SUCCESS=10024;

    public final static int SQUARE_MESSAGE_PRAISE_FAILURE=10025;

    public final static int GET_INTEREST_TYPE_SUCCESS=10026;

    public final static int GET_INTEREST_TYPE_FAILURE=10027;

    public final static int GET_INTEREST_TYPE_LIST_SUCCESS=10028;

    public final static int GET_INTEREST_TYPE_LIST_FAILURE=10029;

    public final static int ADD_INTEREST_SUCCESS=10030;

    public final static int ADD_INTEREST_FAILURE=10031;

    public final static int FORGET_PASSWORD_SUCCESS=10032;

    public final static int FORGET_PASSWORD_FAILURE=10033;

    public final static int GET_CHAT_USER_SUCCESS=10034;

    public final static int GET_CHAT_USER_FAILURE=10035;

    public final static int SEND_CHAT_MESSAGE_SUCCESS=10036;

    public final static int SEND_CHAT_MESSAGE_FAILURE=10037;


    public final static int GET_CHAT_RECEIVER_MESSAGE_SUCCESS=10038;

    public final static int GET_CHAT_RECEIVER_MESSAGE_FAILURE=10039;

    public final static int GET_WEBSOCKET_INFO=10040;

    public final static int SQUARE_MESSAGE_CANCEL_PRAISE_SUCCESS=10041;

    public final static int SQUARE_MESSAGE_CANCEL_PRAISE_FAILURE=10042;

    public final static int GET_USER_INTEREST_SUCCESS=10043;

    public final static int GET_USER_INTEREST_FAILURE=10044;

    public final static int GET_ALL_INTEREST_SUCCESS=10045;

    public final static int GET_ALL_INTEREST_FAILURE=10046;

    public final static int DELETE_USER_INTEREST_SUCCESS=10047;

    public final static int DELETE_USER_INTEREST_FAILURE=10048;

    public final static int RECEIVER_COMMENT_NOTIFICATION_SUCCESS=10049;

    public final static int RECEIVER_PRAISE_NOTIFICATION_SUCCESS=10050;

    public final static int SEND_ALIVE_MESSAGE=10051;

    public final static int ACCEPT_CHAT=10052;

    public final static int RECEIVER_COMMENT_WITH_COMMENT_NOTIFICATION=10053;

    public final static int RECEIVER_COMMENT_WITH_SQUAREMESSAGE_NOTIFICATION=10054;


    public final static int FINISH_CHAT=10055;

    public final static int TARGET_ACCEPT_CHAT=10056;//对方确认聊天

    public final static int TARGET_REJECT_CHAT=10057;//对方拒绝聊天
}
