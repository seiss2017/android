package com.lbl.tempchat.user.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.lbl.tempchat.R;
import com.lbl.tempchat.common.database.entity.CommentMessage;
import com.lbl.tempchat.common.database.entity.SquareMessage;
import com.lbl.tempchat.common.database.entity.UserInfoMessage;
import com.lbl.tempchat.common.request.entity.Comment;
import com.lbl.tempchat.common.request.entity.Interest;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by 13616 on 2017/4/9.
 */

public class MessageCommentAdapter extends BaseAdapter {
    private List<CommentMessage> commentList;
    private Activity activity;
    public MessageCommentAdapter(Activity activity){
        this.activity=activity;
        commentList=new ArrayList<>();
    }

    public void add(List<CommentMessage> commentList){
        this.commentList=commentList;
        Collections.sort(this.commentList);
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*ViewHolder viewHolder;
        if(convertView!=null){
            viewHolder=(ViewHolder)convertView.getTag();
        }else {
            convertView= LayoutInflater.from(activity).inflate(R.layout.item_message_comment, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }*/
        View rootView=LayoutInflater.from(activity).inflate(R.layout.item_message_comment, null);
        CircleImageView iv_comment_head=(CircleImageView)rootView.findViewById(R.id.iv_comment_head);
        TextView tv_comment_nickname=(TextView) rootView.findViewById(R.id.tv_comment_nickname);
        TextView tv_comment_content=(TextView) rootView.findViewById(R.id.tv_comment_content);
        TextView tv_comment_time=(TextView) rootView.findViewById(R.id.tv_comment_time);
        CircleImageView iv_message_comment_user_head=(CircleImageView)rootView.findViewById(R.id.iv_message_comment_user_head);
        TextView tv_message_comment_user_nickname=(TextView) rootView.findViewById(R.id.tv_message_comment_user_nickname);
        TextView tv_message_comment_user_content=(TextView) rootView.findViewById(R.id.tv_message_comment_user_content);

        CommentMessage commentMessage=commentList.get(position);
        UserInfoMessage reviewer=(UserInfoMessage)new Select().from(UserInfoMessage.class).where("id=?",commentMessage.reviewer).execute().get(0);
        if(reviewer.avatar!=null&&!reviewer.avatar.equals("")){
            Picasso.with(activity).load(reviewer.avatar).into(iv_comment_head);
        }
        tv_comment_nickname.setText(reviewer.nickname);
        tv_comment_content.setText(commentMessage.content);
        tv_comment_time.setText(commentMessage.time.replace("T"," "));
        if(commentMessage.pointTo!=-1){
            Log.i("lbl","pointToValue"+commentMessage.pointTo);
            UserInfoMessage pointTo=(UserInfoMessage)new Select().from(UserInfoMessage.class).where("id=?",commentMessage.pointTo).execute().get(0);
            CommentMessage beReplyed=(CommentMessage)new Select().from(CommentMessage.class).where("id=?",commentMessage.source).execute().get(0);

            if(pointTo.avatar!=null&&!pointTo.avatar.equals("")){
                Picasso.with(activity).load(reviewer.avatar).into(iv_message_comment_user_head);
            }
            tv_message_comment_user_nickname.setText(pointTo.nickname);
            tv_message_comment_user_content.setText(beReplyed.content);

        }else {
            SquareMessage squareMessage=(SquareMessage)new Select().from(SquareMessage.class).where("id=?",commentMessage.source).execute().get(0);
            UserInfoMessage poster=(UserInfoMessage)new Select().from(UserInfoMessage.class).where("id=?",squareMessage.poster).execute().get(0);
            if(poster.avatar!=null&&!poster.avatar.equals("")){
                Picasso.with(activity).load(reviewer.avatar).into(iv_message_comment_user_head);
            }
            tv_message_comment_user_nickname.setText(poster.nickname);
            tv_message_comment_user_content.setText(squareMessage.content);
        }

       /* if(comment.getReviewer().getAvatar()!=null&&!comment.getReviewer().getAvatar().equals("")){
            Picasso.with(activity).load(comment.getReviewer().getAvatar()).into(viewHolder.iv_comment_head);
        }
        viewHolder.tv_comment_nickname.setText(comment.getReviewer().getNickname());
        viewHolder.tv_comment_content.setText(comment.getContent());
        viewHolder.tv_comment_time.setText(comment.getTime().replace("T"," "));
        if(comment.getPointTo().getAvatar()!=null&&!comment.getPointTo().getAvatar().equals("")){
            Picasso.with(activity).load(comment.getPointTo().getAvatar()).into(viewHolder.iv_message_comment_user_head);
        }

        viewHolder.tv_message_comment_user_nickname.setText(comment.getPointTo().getNickname());

        //TODO:
        viewHolder.tv_message_comment_user_content.setText("这不是一条消息");*/
        return rootView;
    }

    class ViewHolder{
        @BindView(R.id.iv_comment_head)
        CircleImageView iv_comment_head;

        @BindView(R.id.tv_comment_nickname)
        TextView tv_comment_nickname;

        @BindView(R.id.tv_comment_content)
        TextView tv_comment_content;

        @BindView(R.id.tv_comment_time)
        TextView tv_comment_time;

        @BindView(R.id.iv_message_comment_user_head)
        CircleImageView iv_message_comment_user_head;

        @BindView(R.id.tv_message_comment_user_nickname)
        TextView tv_message_comment_user_nickname;

        @BindView(R.id.tv_message_comment_user_content)
        TextView tv_message_comment_user_content;

        public ViewHolder(View view){
            ButterKnife.bind(this,view);
        }
    }
}
