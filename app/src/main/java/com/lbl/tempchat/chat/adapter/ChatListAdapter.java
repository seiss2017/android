package com.lbl.tempchat.chat.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lbl.tempchat.R;
import com.lbl.tempchat.chat.entity.ChatListMessage;
import com.lbl.tempchat.common.request.entity.ChatMessage;
import com.lbl.tempchat.common.request.entity.ChatSendMessage;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.square.adapter.MomentAdapter;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by 13616 on 2017/5/4.
 */

public class ChatListAdapter extends BaseAdapter {

    private ArrayList<ChatListMessage>  chatMessageList;

    private Activity activity;

    private UserInfo sender;


    //用户已发送但还没有确认是否已经发送成功的信息
    private ChatListMessage waitToAdd;


    public void setWaitToAdd(ChatListMessage chatSendMessage){
        this.waitToAdd=chatSendMessage;
    }

    public void setSender(UserInfo sender){
        this.sender=sender;
    }

    public void addWaitToList(){
        if(waitToAdd==null){
            return;
        }
        add(waitToAdd);
        waitToAdd=null;


    }

    public ChatListAdapter(Activity activity){
        this.activity=activity;
        this.chatMessageList=new ArrayList<>();
    }

    public void add(ChatListMessage chatListMessage){
        chatMessageList.add(chatListMessage);
        //Collections.sort(chatMessageList);

    }


    @Override
    public int getCount() {
        return chatMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView!=null){
            viewHolder=(ViewHolder)convertView.getTag();
        }else {
            convertView= LayoutInflater.from(activity).inflate(R.layout.item_chat_message, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        ChatListMessage chatListMessage=chatMessageList.get(position);
        if(chatListMessage.getType()==ChatListMessage.USER_RECEIVER){//接收信息时
            viewHolder.left_layout.setVisibility(View.VISIBLE);
            viewHolder.right_layout.setVisibility(View.GONE);
            viewHolder.iv_message_left_head.setVisibility(View.VISIBLE);
            viewHolder.iv_message_right_head.setVisibility(View.GONE);
            viewHolder.left_msg.setText(chatListMessage.getContent());
            if(chatListMessage.getUserInfo()!=null
                    &&chatListMessage.getUserInfo().getAvatar()!=null
                    &&!chatListMessage.getUserInfo().getAvatar().equals("")){

                Picasso.with(activity).load(chatListMessage.getUserInfo().getAvatar()).into(viewHolder.iv_message_left_head);
            }
        }else {//用户发送的消息
            viewHolder.left_layout.setVisibility(View.GONE);
            viewHolder.right_layout.setVisibility(View.VISIBLE);
            viewHolder.iv_message_left_head.setVisibility(View.GONE);
            viewHolder.iv_message_right_head.setVisibility(View.VISIBLE);
            viewHolder.right_msg.setText(chatListMessage.getContent());
            if(sender!=null
                    &&sender.getAvatar()!=null
                    &&!sender.getAvatar().equals("")){
                Picasso.with(activity).load(sender.getAvatar()).into(viewHolder.iv_message_right_head);
            }

        }

        return convertView;


    }

    class ViewHolder{
        @BindView(R.id.left_layout)
        RelativeLayout left_layout;

        @BindView(R.id.right_layout)
        RelativeLayout right_layout;

        @BindView(R.id.left_msg)
        TextView left_msg;

        @BindView(R.id.right_msg)
        TextView right_msg;

        @BindView(R.id.iv_message_right_head)
        CircleImageView iv_message_right_head;

        @BindView(R.id.iv_message_left_head)
        CircleImageView iv_message_left_head;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
