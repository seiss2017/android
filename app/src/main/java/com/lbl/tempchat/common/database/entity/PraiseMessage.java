package com.lbl.tempchat.common.database.entity;

import android.graphics.PorterDuff;
import android.support.annotation.NonNull;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.lbl.tempchat.common.request.entity.SquareMessage;
import com.lbl.tempchat.common.request.entity.UserInfo;
import com.lbl.tempchat.utils.MyDateUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 13616 on 2017/5/8.
 */
@Table(name="praise")
public class PraiseMessage extends Model implements Serializable,Comparable {
    public PraiseMessage(){}

    public PraiseMessage(com.lbl.tempchat.common.request.entity.PraiseMessage squareMessage){
        this.images=squareMessage.getImages();
        this.id=squareMessage.getId();
        this.time=squareMessage.getTime();
        this.poster=new UserInfoMessage(squareMessage.getPoster()).save();
        this.content=squareMessage.getContent();
        this.comments=squareMessage.getComments();
        this.likes=squareMessage.getLikes();
        this.hasLiked=squareMessage.isHasLiked();
        this.liker=new UserInfoMessage(squareMessage.getLiker()).save();
    }

    //@Column(name = "images")
    public ArrayList<String> images;

    @Column(name="praiseId")
    public int id;

    @Column(name ="time")
    public String time;

    @Column(name="poster")
    public long poster;

    @Column(name="content")
    public String content;

    @Column(name = "comments")
    public int comments;

    @Column(name="likes")
    public int likes;

    @Column(name = "hasLiked")
    public boolean hasLiked;

    @Column(name="liker")
    public long liker;

    @Override
    public int compareTo(@NonNull Object o) {
        Date date1= MyDateUtils.formatStringToDate(time);
        Date date2= MyDateUtils.formatStringToDate(((PraiseMessage)o).time);
        return date1.getTime()>date2.getTime()?-1:1;
    }

}
