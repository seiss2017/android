package com.lbl.tempchat.common.request;

/**
 * Created by 13616 on 2017/5/4.
 */

public class SocketTypeCode {
    public static final int SEND_CHAT=1;
    public static final int ACCEPT_CHAT=2;
    public static final int SEND_ALIVE=3;
    public static final int STOP_CHAT=4;
    public static final int REQUEST_CHAT=5;



    public static final int REQUEST_STATUS =1;

    public static final int RECEIVER_CHAT=2;

    public static final int RECEIVER_COMMENT=3;

    public static final int RECEIVER_PRAISE=4;

    public static final int CHAT_SURE=5;//会话确认

    public static final int ALIVE_ENSURE=6;



}
