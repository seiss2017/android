package com.lbl.tempchat.user.activity;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.user.adapter.InterestAdapter;
import com.lbl.tempchat.user.info.User;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.toolbar_setting)
    Toolbar toolbar;

    private OkHttpClient httpClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }

    @Override
    protected void initVariables(){

        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();

    }

    @Override
    protected void initViews(){
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        //设置返回键
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("设置");
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void loadData(){
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }

    @OnClick(R.id.btn_setting_logout)
    protected void logout(){

    }

}
