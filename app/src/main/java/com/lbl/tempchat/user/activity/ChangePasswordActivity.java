package com.lbl.tempchat.user.activity;

import android.graphics.Color;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.lbl.tempchat.R;
import com.lbl.tempchat.common.activity.BaseActivity;
import com.lbl.tempchat.common.handler.RequestHandler;
import com.lbl.tempchat.common.request.RequestBase;
import com.lbl.tempchat.common.request.RequestCode;
import com.lbl.tempchat.login.activity.RegisterActivity;
import com.lbl.tempchat.user.info.User;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class ChangePasswordActivity extends BaseActivity {

    @BindView(R.id.tiet_change_password_before_password)
    TextInputEditText tiet_change_password_before_password;

    @BindView(R.id.tiet_change_password_password)
    TextInputEditText tiet_change_password_password;

    @BindView(R.id.tiet_change_password_new_password_again)
    TextInputEditText tiet_change_password_new_password_again;

    @BindView(R.id.tiet_change_password_check)
    TextInputEditText tiet_change_password_check;


    @BindView(R.id.btn_change_password_request_check)
    Button btn_change_password_request_check;

    private static final int CHECKER_TIME_DOWN=1000;

    private static final int CHECKER_CAN_GET=1001;

    private RequestBase<String> requestChecker;

    private RequestBase<String> requestChangePassword;
    private OkHttpClient httpClient;


    private RequestHandler handler=new RequestHandler(this){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RequestCode.GET_CHECKER_SUCCESS:
                    User.getUser().setUserCipher(ChangePasswordActivity.this,requestChecker.getStatus().getData());
                    break;
                case RequestCode.GET_CHECKER_FAILURE:
                    Toast.makeText(ChangePasswordActivity.this,"获取验证码失败,请稍后重试",Toast.LENGTH_SHORT).show();
                    break;
                case RequestCode.CHANGE_PASSWORD_SUCCESS:
                    new AlertDialog.Builder(ChangePasswordActivity.this)
                            .setTitle("修改密码成功")
                            .setPositiveButton("确定", (a,b)->{a.dismiss();ChangePasswordActivity.this.finish();})
                            .create()
                            .show();
                    break;
                case RequestCode.CHANGE_PASSWORD_FAILURE:
                    Toast.makeText(ChangePasswordActivity.this,"修改密码失败",Toast.LENGTH_SHORT).show();
                    break;
                case CHECKER_TIME_DOWN:
                    btn_change_password_request_check.setText("验证码("+msg.arg1+"s)");
                    break;
                case CHECKER_CAN_GET:
                    btn_change_password_request_check.setEnabled(true);
                    btn_change_password_request_check.setText("验证码");
                    btn_change_password_request_check.setBackground(getDrawable(R.drawable.btn_circleview));
                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        ButterKnife.bind(this);

        initVariables();
        initViews();
        loadData();
    }

    @Override
    protected void initVariables(){
        requestChecker=new RequestBase<>(getString(R.string.url_checker),
                RequestCode.GET_CHECKER_SUCCESS,
                RequestCode.GET_CHECKER_FAILURE);

        requestChangePassword=new RequestBase<>(getString(R.string.url_change_password),
                RequestCode.CHANGE_PASSWORD_SUCCESS,
                RequestCode.CHANGE_PASSWORD_FAILURE);
        httpClient=new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build();
    }

    @Override
    protected void initViews(){
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_change_password);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar=this.getSupportActionBar();
        actionBar.setTitle("修改密码");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void loadData(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.btn_change_password_request_check)
    public void requestChecker(){
        String email=User.getUser().getUserEmail(this);
        if(email.equals(User.DATA_NO_EXIST)){
            Toast.makeText(this,"数据异常,请稍后再试",Toast.LENGTH_SHORT).show();
        }
        HashMap<String,String> map=new HashMap<>();
        map.put("email",email);
        btn_change_password_request_check.setEnabled(false);
        requestChecker.requestPost(this,handler,httpClient,map);
        btn_change_password_request_check.setBackground(getDrawable(R.drawable.btn_circleview_grey));
        new Thread(()->{
            for(int i=60;i>=0;i--){
                Message msg=new Message();
                msg.what=CHECKER_TIME_DOWN;
                msg.arg1=i;
                handler.sendMessage(msg);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            handler.sendEmptyMessage(CHECKER_CAN_GET);
        }).start();
    }

    @OnClick(R.id.btn_change_password)
    public void changePassword(){
        String old=tiet_change_password_before_password.getText().toString();
        String newPwd=tiet_change_password_password.getText().toString();
        String newPwdAgain=tiet_change_password_new_password_again.getText().toString();
        String checker=tiet_change_password_check.getText().toString();
        if(old.equals("")
                ||newPwd.equals("")
                ||newPwdAgain.equals("")
                ||checker.equals("")){
            Toast.makeText(this,"请填写完整",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!newPwd.equals(newPwdAgain)){
            Toast.makeText(this,"两次密码输入不一致",Toast.LENGTH_SHORT).show();
            return;
        }
        if(newPwd.length()<6||newPwd.length()>20){
            Toast.makeText(this,"密码长度应为6-20位",Toast.LENGTH_SHORT).show();
            return;
        }
        String cipher=User.getUser().getUserCipher(this);
        HashMap<String,String> map=new HashMap<>();
        map.put("token",User.getUser().getUserToken(this));
        map.put("old",old);
        map.put("current",newPwd);
        map.put("checker",checker);
        map.put("cipher",cipher);
        requestChangePassword.requestPost(this,handler,httpClient,map);
    }


}

