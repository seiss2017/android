package com.lbl.tempchat.common.request;

import java.io.Serializable;

/**
 * Created by 13616 on 2017/5/4.
 */

public class StatusSocket <T> extends Status<T>implements Serializable {

    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
