package com.lbl.tempchat.common.request.entity;

import android.support.annotation.NonNull;

import com.lbl.tempchat.utils.MyDateUtils;

import java.io.Serializable;

/**
 * Created by 13616 on 2017/5/4.
 */

public class ChatMessage implements Serializable, Comparable{
    private UserInfo from;

    private String content;

    private String sendTime;


    public UserInfo getFrom() {
        return from;
    }

    public void setFrom(UserInfo from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        ChatMessage other=(ChatMessage)o;
        return MyDateUtils.formatStringToDate(sendTime).getTime()
                >MyDateUtils.formatStringToDate(other.getSendTime()).getTime()
                ?1:-1;
    }
}
